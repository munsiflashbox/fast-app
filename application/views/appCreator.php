<?php
/**
 * Created by JetBrains PhpStorm.
 * User: admin
 * Date: 23/1/13
 * Time: 12:11 PM
 * To change this template use File | Settings | File Templates.
 */
?>

<!Doctype html>
 <html>
    <head>
        <title>
            Flapp
        </title>

<!--Styles-->
        <link rel = "stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/styles/style.css" />
        <link rel = "stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/styles/jquery-ui.css" />
    </head>
    <body>
        <div id = "mainWrapper">
            <div class = "header">
                <div class = "title">
                    FLAPP!
                </div>

                <div class = "clear"></div>
            </div>

            <div class = "appArea">
                <div class = "layers">
                    <div class = "elements">
                        <div class = "square"></div>
                        <div class = "addOpacity"></div>
                        <div class = "addColor"></div>
                        <div class = "addRound"></div>
                    </div>

                    <div class = "clear"></div>
                </div>

                <div class = "buildArea">
                    <div class = "screenArea">

                    </div>
                    <ul class = "props"></ul>
                    <ul class = "vals"></ul>
                </div>

                <div class = "controlArea bor">
                    <div class = "controls">
                        <div class = "record" title="Record">
                        </div>

                        <div class = "stop" title="Stop">
                        </div>

                        <div class = "play" title="Play Selected">
                        </div>

                        <div class = "playAll" title="Play All">
                        </div>
                    </div>
                </div>

                <div class = "clear"></div>
            </div><!--App Area Ends-->

            <div class = "clear"></div>
        </div><!--Main Wrapper Ends-->

    <!--Scripts Area-->
        <script data-cfasync="true" type = "text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script data-cfasync="true" type = "text/javascript" src="<?php echo base_url(); ?>assets/scripts/plugins.js"></script>
        <script data-cfasync="true" type = "text/javascript" src="<?php echo base_url(); ?>assets/scripts/flapp.js"></script>
    <script type = "text/javascript">
        $(function(){
            flapp.setEvents();
        })
    </script>
    </body>
 </html>
