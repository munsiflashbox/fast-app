<!Doctype html>
<html>
<head>
    <title>
        ColorZap
    </title>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/styles/generic.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/styles/custom-ui.css"/>
<head>
<body>
<div id="mainWrap">
    <div class="leftMenu floatLeft">
        <div class="leftMenuContent">
            <div class="shapes leftTool">
                <div class="shapesImg"></div>
                <div class="shapesTxt">
					<span class="txt">Shapes<span>
                </div>
            </div>
            <div class="images leftTool">
                <div class="ImagesImg"></div>
                <div class="shapesTxt">
					<span class="txt">Images<span>
                </div>
            </div>
            <div class="text leftTool">
                <div class="textImg"></div>
                <div class="shapesTxt">
					<span class="txt">Text<span>
                </div>
            </div>
            <div class="clr">
            </div>
        </div>
    </div>
    <div class="drawingArea floatLeft">
        <div class="topMenu1">
            <div class="topMenuContent1 shapesHolder">
                <div class="addSquare"></div>
                <div class="addCircle"></div>
                <div class="addtriangle"></div>

                <div class="clr">
                </div>
            </div>
            <div class="topMenuContent1 textEditor">
                <div class = "editorContainer">
                <div class = "editorDivs fontSelection">
                    <div class = "selectedFont">Font</div>
                    <div class = "fontDropDown">
                        <div class = "selectableFont">TMR</div>
                        <div class = "selectableFont">Verdana</div>
                        <div class = "selectableFont">Arial</div>
                        <div class = "selectableFont">Tahoma</div>
                        <div class = "selectableFont">Courier New</div>
                        <div class = "selectableFont">San-Serif</div>
                        <div class = "selectableFont">Droid Sans</div>
                        <div class = "selectableFont">Chiller</div>
                    </div>
                </div>
<!--Fonts End-->

                <div class = "editorDivs boldFont")">
                </div>

                <div class = "editorDivs italicFont">
                </div>

                <div class = "editorDivs fontColor"></div>
                    <div class="fColorSel">
                            <div id="colorPallete">
                                <div class='levels'>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#000000')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col1.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#abafb2')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col2.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#5c3d21')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col3.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#7f542a')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col4.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#f8e9ca')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col5.png"/>

                                    <div class="clear"></div>
                                </div>
                                <div class='levels'>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#01455a')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col6.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#01b4b9')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col7.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#003e87')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col8.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#72a1b1')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col9.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#b2d8ed')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col10.png"/>

                                    <div class="clear"></div>
                                </div>
                                <div class='levels'>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#013660')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col11.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#223764')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col12.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#682376')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col13.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#cac6df')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col14.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#8da8c5')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col15.png"/>

                                    <div class="clear"></div>
                                </div>
                                <div class='levels'>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#731d3a')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col16.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#9c1e29')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col17.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#ef1901')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col18.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#ef4689')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col19.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#fed5dd')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col20.png"/>

                                    <div class="clear"></div>
                                </div>
                                <div class='levels'>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#b46115')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col21.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#f7593e')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col22.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#ff8500')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col23.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#fd7d64')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col24.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#ffcc4d')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col25.png"/>

                                    <div class="clear"></div>
                                </div>
                                <div class='levels'>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#f6c101')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col26.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#fff100')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col27.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#fef79c')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col28.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#d9c600')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col29.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#a0a028')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col30.png"/>

                                    <div class="clear"></div>
                                </div>
                                <div class='levels'>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#0d3922')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col31.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#015e4c')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col32.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#258031')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col33.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#929f85')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col34.png"/>
                                    <img class="color" title="Clean" onclick="formatDoc('forecolor','#d4e1a9')"
                                         src="<?php echo base_url(); ?>assets/images/colorPallets/col35.png"/>

                                    <div class="clear"></div>
                                </div>
                            </div>
                    </div>

                <div class = "editorDivs sizeSelect">
                    <div class = "fontSize"></div>
                    <div class = "fontSizeSelection">
                        <div class = "selectSize">1</div>
                        <div class = "selectSize">2</div>
                        <div class = "selectSize">3</div>
                        <div class = "selectSize">4</div>
                        <div class = "selectSize">5</div>
                        <div class = "selectSize">6</div>
                        <div class = "selectSize">7</div>
                    </div>
                    <div class = "clear"></div>
                </div>

                <div class="clr">
                </div>
            </div>
            </div>
        </div>


        <div class="containerDiv">
            <div class="topMenu2">
                <div class="topMenuContent2">
                    <div class="colored edit floatLeft ">
                        Color
                    </div>
                    <div class="rotate edit floatLeft">
                        Rotate
                    </div>
                    <div class="resize edit floatLeft">
                        Alpha
                    </div>
                </div>

                <div class = "topMenuTools">
                    <div class="pallete tool">
                        <div id="bgPallete">
                            <div class='levels'>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#000000')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col1.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#abafb2')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col2.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#5c3d21')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col3.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#7f542a')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col4.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#f8e9ca')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col5.png"/>

                                <div class="clear"></div>
                            </div>
                            <div class='levels'>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#01455a')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col6.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#01b4b9')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col7.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#003e87')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col8.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#72a1b1')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col9.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#b2d8ed')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col10.png"/>

                                <div class="clear"></div>
                            </div>
                            <div class='levels'>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#013660')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col11.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#223764')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col12.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#682376')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col13.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#cac6df')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col14.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#8da8c5')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col15.png"/>

                                <div class="clear"></div>
                            </div>
                            <div class='levels'>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#731d3a')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col16.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#9c1e29')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col17.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#ef1901')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col18.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#ef4689')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col19.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#fed5dd')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col20.png"/>

                                <div class="clear"></div>
                            </div>
                            <div class='levels'>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#b46115')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col21.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#f7593e')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col22.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#ff8500')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col23.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#fd7d64')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col24.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#ffcc4d')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col25.png"/>

                                <div class="clear"></div>
                            </div>
                            <div class='levels'>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#f6c101')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col26.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#fff100')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col27.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#fef79c')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col28.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#d9c600')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col29.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#a0a028')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col30.png"/>

                                <div class="clear"></div>
                            </div>
                            <div class='levels'>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#0d3922')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col31.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#015e4c')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col32.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#258031')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col33.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#929f85')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col34.png"/>
                                <img class="color" title="Clean" onclick="formatBG('forecolor','#d4e1a9')"
                                     src="<?php echo base_url(); ?>assets/images/colorPallets/col35.png"/>

                                <div class="clear"></div>
                            </div>
                        </div>
                        </div>
                    <div class = "rotateTool tool">
                        <div class = "rotateSlider"></div>
                    </div>
                    <div class = "alphaTool tool">
                        <div class = "alphaSlider"></div>
                    </div>
                </div>
            </div>
            <div class="workSpace">
                <div class = "screenArea">

                </div>
            </div>
            <div class="control">
                <div class="controlElement">
                    <div class="recStop floatLeft" style = "background: url('assets/images/record.png') no-repeat center;"></div>
                    <div class="play floatLeft"></div>
                    <div class="pause floatLeft"></div>

                    <div class = "timeLine">
                        <div class = "lineSlider"></div>
                    </div>

                    <div class="clr"></div>
                </div>
            </div>

            <div class="stop"></div>
        </div>
    </div>
    <div class="rightMenu floatLeft">
        <div class="publish">
            <span>PUBLISH</span>
        </div>
        <div class="creatLayer">
            Layers
            <span>+ New</span>
        </div>
        <div class="layersContent">
            <div class="layerOne">
                Layer1
            </div>
            <ul>
                <li><img src="<?php echo base_url(); ?>assets/images/active.png">Shape1</li>
                <li><img src="<?php echo base_url(); ?>assets/images/active.png">Shape1</li>
                <li><img src="<?php echo base_url(); ?>assets/images/active.png">Shape1</li>
            </ul>
        </div>
    </div>
    <div class="clr">
    </div>
</div>

<!--Scripts Area-->
<script data-cfasync="true" type = "text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script data-cfasync="true" type = "text/javascript" src="<?php echo base_url(); ?>assets/scripts/plugins.js"></script>
<script data-cfasync="true" type = "text/javascript" src="<?php echo base_url(); ?>assets/scripts/transition.js"></script>
<script data-cfasync="true" type = "text/javascript" src="<?php echo base_url(); ?>assets/scripts/flapp.js"></script>
<script type = "text/javascript">
    $(function(){
        flapp.setEvents();
    })
</script>
</body>
</html>