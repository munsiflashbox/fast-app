/*
    @Author Munsif
    Flapp JS v 1.0
*/
var oDoc, sDefTxt;
function initDoc(newId) {
    oDoc = document.getElementById(newId);
    sDefTxt = oDoc.innerHTML;
}

function formatDoc(sCmd, sValue) {
    document.execCommand(sCmd, false, sValue);
    oDoc.focus();
}
function formatBG(value1, value2) {
      $(flapp.objectId).css("background",value2);
      $(flapp.objectId).data("background",value2);
}

var flapp = {
    objectId:'',
    squareCount:0,
    circleCount:0,
    textCount:0,
    itemIndex:0,
    itemNum:0,
    urlPathPrefix : "../assets/images/",

    //Adding Elements
    addSquare: function(){
        $('.addSquare').click(function(){
//        $('.square').click(function(){
            flapp.squareCount++;
            flapp.itemNum++
            flapp.itemIndex++;
            $('<div class ="squareClone"></div>').appendTo('.screenArea')
                .attr("id","squareNo"+flapp.squareCount)
                .attr("data-type","element")
                .attr("data-object","square")
                .attr("data-delay",parseInt(flapp.itemNum*1000, 10))
                .attr("data-itemno",parseInt(flapp.itemNum, 10))
                .attr("data-rotateold","0deg")
                .css({
                    "width":"200px",
                    "height":"200px",
                    "position":"absolute",
                    "opacity":"1",
                    "background":"black",
                    "color":"#ffffff",
                    zIndex:flapp.itemIndex
                })
                .draggable({
                    cursor: "move",
                    drag:function(){
//                        $(this).html("X : "+$(this).offset().left+ " Y : "+$(this).offset().top);
                    }
                })
                .resizable({
                    handles:'n,s,e,w,ne,nw,se,sw'
                })
                .append('<div class="drag-corner ui-draggable-n"></div><div class="drag-corner ui-draggable-w"></div><div class="drag-corner ui-draggable-e"></div><div class="drag-corner ui-draggable-s"></div>')

            //Set the Current Id for objId
            flapp.objectId = "#squareNo"+flapp.squareCount;
        });
    },
    addCircle: function(){
        $('.addCircle').click(function(){
//        $('.square').click(function(){
            flapp.squareCount++;
            flapp.itemNum++;
            flapp.itemIndex++;
            flapp.itemNum++;
            $('<div class ="circleClone"></div>').appendTo('.screenArea')
                .attr("id","circle"+flapp.circleCount)
                .attr("data-type","element")
                .attr("data-object","circle")
                .attr("data-delay",parseInt(flapp.itemNum*1000, 10))
                .attr("data-itemno",parseInt(flapp.itemNum, 10))
                .attr("data-rotateold","0deg")
                .css({
                    "width":"200px",
                    "height":"200px",
                    "position":"absolute",
                    "borderRadius":"100%",
                    "opacity":"1",
                    "background":"black",
                    "color":"#ffffff",
                    zIndex:flapp.itemIndex
                })
                .draggable({
                    cursor: "move",
                    drag:function(){
//                        $(this).html("X : "+$(this).offset().left+ " Y : "+$(this).offset().top);
                    }
                })
                .resizable({
                    handles:'ne,nw,se,sw',
                   aspectRatio: 1
                })
                .append('<div class="drag-corner ui-draggable-n"></div><div class="drag-corner ui-draggable-w"></div><div class="drag-corner ui-draggable-e"></div><div class="drag-corner ui-draggable-s"></div>')
            //Set the Current objId
            flapp.objectId = "#circle"+flapp.circleCount;
        });
    },

    addText:function(){
        $('.text').click(function(){
            flapp.textCount++;
            flapp.itemIndex++;
            flapp.itemNum++;
            $('<div class ="textArea"><div class = "thisCont"></div></div>').appendTo('.screenArea')
                .attr("id","textArea"+flapp.textCount)
                .attr("data-type","element")
                .attr("data-object","text")
                .attr("onclick","initDoc('textArea"+flapp.textCount+"')")
                .attr("data-delay",parseInt(flapp.itemNum*1000, 10))
                .attr("data-itemno",parseInt(flapp.itemNum, 10))
                .css({
                    position: "absolute",
                    zIndex:flapp.itemIndex
                })
                .draggable({
                    cursor: "move",
                    drag:function(){
//                        $(this).html("X : "+$(this).offset().left+ " Y : "+$(this).offset().top);
                    }
                })
                .resizable({
                    handles:'n,s,e,w,ne,nw,se,sw'
                })
                .append('<div class="drag-corner ui-draggable-n"></div><div class="drag-corner ui-draggable-w"></div><div class="drag-corner ui-draggable-e"></div><div class="drag-corner ui-draggable-s"></div>');

            $("#textArea"+flapp.textCount+" .thisCont")
                .html("Fast Text for FastApp<br/>To quickly locate topics on a particular subject in the documentation, enter a query in the Search field. Use the Search frame to display the Search view. You can narrow ")
                .css({
                    height:"inherit",
                    width:"inherit",
                    overflow:"hidden",
                    fontFamily:"chiller"
                })

        });
        //Set the Current objId
        flapp.objectId = "#textArea"+flapp.textCount;

        flapp.editText();
    },
    editText:function(){
        $('[data-object="text"]').live('dblclick',function(){
            $(this).draggable("disable");
            $(this).attr("contenteditable","true");
        });

        //Bold
        $('.boldFont').on('click',function(){
            formatDoc("bold","")
        });
        //Italic
        $('.italicFont').on('click',function(){
            formatDoc("italic","")
        });
        //Size
        $('.selectSize').click(function(){
            formatDoc("fontsize",$(this).html())
        });
        //Family
        $('.selectableFont').click(function(){
            formatDoc("fontname",$(this).html())
        });
    },
    uiStuff:function(){
        $('[data-type="element"]').live('mousedown', function(){
            $('.drag-corner').hide();
            $(this).find('.drag-corner').show();


            //Toggle Tool Bar
             var object = $(this).data("object");
            switch(object){
                case 'text':
                    $('.topMenu1').find('.topMenuContent1').hide();
                    $('.topMenu1').find('.textEditor').show();
                    $('[data-type = "element"]').resizable('destroy');
                    $(this).resizable({
                        handles:'n,e,w,s,nw, sw, se, ne'
                    });
                    break;
                case 'square':
                    $('[data-type = "element"]').resizable('destroy');
                    $(this).resizable({
                        handles:'n,e,w,s,nw, sw, se, ne'
                    });
                    $('.topMenu1').find('.topMenuContent1').hide();
                    $('.topMenu1').find('.shapesHolder').show();
                    break;
                case 'circle':
                    $('[data-type = "element"]').resizable('destroy');
                    $(this).resizable({
                        handles:'nw, sw, se, ne',
                        aspectRatio:1
                    });
                    $('.topMenu1').find('.topMenuContent1').hide();
                    $('.topMenu1').find('.shapesHolder').show();
                    break;
            }
        });

        $('.screenArea').click(function(e){
//            console.log(e.target)
            if(e.target === $('.screenArea')[0]){
                $('[data-type = "element"]').resizable("destroy");
                $('.drag-corner').hide();
                $('[data-type = "element"]').attr("contenteditable","fasle");
                $('[data-type = "element"]').draggable("enable");

                $('.topMenu1').find('.topMenuContent1').hide();
                $('.topMenu1').find('.shapesHolder').show();
            }
        });
        $('.shapes').click(function(){
           $('.topMenu1').find('.topMenuContent1').hide();
            $('.topMenu1').find('.shapesHolder').show();
        });

    },

    //Accessing the Elements
    accessElem:function(){
        $('[data-type="element"]').live('mousedown',function(){
            flapp.objectId = '#'+$(this).attr("id");
        });
    },

    //All the cool stuff here
    recordStop:function(){
        $('.recStop').toggle(function(){
            $(flapp.objectId)
                .data("xone",$(flapp.objectId).offset().left)
                .data("yone",$(flapp.objectId).offset().top)
                .data("oldWidth",$(flapp.objectId).width())
                .data("oldHeight",$(flapp.objectId).height())
                .data("oldOpacity",$(flapp.objectId).css("opacity"))
                .data("oldBG",$(flapp.objectId).css("backgroundColor"))
                .data("oldRound",$(flapp.objectId).css("borderTopLeftRadius"));
                $(this).css({"background":"url('assets/images/stop.png')"});
//                $(flapp.objectId).html($(flapp.objectId).data("xone")+"<br/>"+$(flapp.objectId).data("yone"));
        }
            ,function(){//STOP functionality
                $(flapp.objectId)
                    .data("left",$(flapp.objectId).offset().left - $('.workSpace').offset().left, 10)
                    .data("top",$(flapp.objectId).offset().top)
                    .data("height",$(flapp.objectId).height())
                    .data("width",$(flapp.objectId).width())
                    .data("opacity",$(flapp.objectId).css("opacity"))
                    .data("backgroundColor",$(flapp.objectId).css("backgroundColor"))
                    .data("borderRadius",$(flapp.objectId).css("borderTopLeftRadius"));
                $(this).css({"background":"url('assets/images/record.png')"});
//                $(flapp.objectId).html($(flapp.objectId).data("top")+"<br/>"+$(flapp.objectId).data("left"));
            });
    },
    record:function(){
        $('.record').live("click",function(){//RECORD Functionality
            $(flapp.objectId)
                .data("xone",$(flapp.objectId).offset().left)
                .data("yone",$(flapp.objectId).offset().top)
                .data("oldWidth",$(flapp.objectId).width())
                .data("oldHeight",$(flapp.objectId).height())
                .data("oldOpacity",$(flapp.objectId).css("opacity"))
                .data("oldBG",$(flapp.objectId).css("backgroundColor"))
                .data("oldRound",$(flapp.objectId).css("borderTopLeftRadius"));

        });
    },
    extraSpace:$('.screenArea').offset().left,
    stop:function(){
        alert(flapp.extraSpace);
        $('.stop').live('click',function(){
            $(flapp.objectId)
                .data("left",parseInt($(flapp.objectId).offset().left - $('.screenArea').offset().left, 10))
                .data("top",$(flapp.objectId).offset().top)
                .data("height",$(flapp.objectId).height())
                .data("width",$(flapp.objectId).width())
                .data("opacity",$(flapp.objectId).css("opacity"))
                .data("backgroundColor",$(flapp.objectId).css("backgroundColor"))
                .data("borderRadius",$(flapp.objectId).css("borderTopLeftRadius"));
        });
    },
    play:function(){
        $('.play').click(function(){
//            $(flapp.objectId).css({
//                "top":$(flapp.objectId).data("yone"),
//                "left":$(flapp.objectId).data("xone")
//            });
//            $(flapp.objectId).html(
//                "Playing From, Top : "+$(flapp.objectId).data("yone")+"<br/>"+
//                "left : "+$(flapp.objectId).data("xone")
//            );
////            alert("Position Reset");
//            $(flapp.objectId).animate({
//                    "top":$(flapp.objectId).data("top"),
//                    "left":$(flapp.objectId).data("left")
//                });
            $(flapp.objectId).css({
                "top":$(this).attr("data-yone"),
                "left":$(this).attr("data-xone")
            });

//            var objid = $(flapp.objectId).attr("id");
            var data = $(flapp.objectId).data();
            var properties = [];
            var vals = [];
            for(var i in data){
                properties.push(i);
                vals.push(data[i]);
            }

            $(".props li").each(function() { properties.push($(this).text()) });
            $(".vals li").each(function() { vals.push($(this).text()) });

            properties.splice(0,1);
            vals.splice(0,1);
//                console.log(properties);
            var anim = {};
            for(var i = 0;  i < properties.length; i++){
                anim[properties[i]] = vals[i];
            }
            console.log(anim);
            $(flapp.objectId).animate(anim);
        });
    },
    playAll:function(){
//        $('.playAll').click(function () {
        $('.play').click(function () {
            $('[data-type ="element"]').resizable('destroy');
            $('.drag-corner').hide();
            $('[data-type="element"]').each(function (){
                $(this).css({
                    "top":$(this).data("yone"),
                    "left":$(this).data("xone")-parseInt($('.workSpace').offset().left, 10),
                    "width":$(this).data("oldWidth"),
                    "height":$(this).data("oldHeight"),
                    "opacity":$(this).data("oldOpacity"),
                    "transform":'rotate('+$(this).data("rotateold")+')',
                    "background":$(this).data("oldBG"),
                    "borderRadius":$(this).data("oldRound")
                });
                var objid = $(this).attr("id");
                var data = $('#'+objid).data();
                var properties = [];
                var vals = [];
                for(var i in data){
                    properties.push(i);
                    vals.push(data[i]);
                }
                properties.splice(0,1);
                vals.splice(0,1);
                var anim = {};
                for(var i = 0;  i < properties.length; i++){
                    anim[properties[i]] = vals[i];
                }

                var itemDelayNow= $(this).data("itemno");
                var itemDelayPrev = itemDelayNow - 1;
                var dlayNow = $('.screenArea').find('[data-itemno="'+itemDelayNow+'"]').data("delay")
                var dlayprev = $('.screenArea').find('[data-itemno="'+itemDelayPrev+'"]').data("delay")
                if(itemDelayPrev > -2){
                    var delayNow = parseInt(dlayNow - dlayprev, 10);
                }
                else{
                    delayNow = 0
//                    $(this).transition(anim, delayNow);
                }
//                alert(delayNow)
                if(delayNow != 1000)
                $(this).transition(anim, 1000);
                else
                $(this).transition(anim, delayNow);

            });
            var t = $('.screenArea').find('[data-type="element"]').length;
            var dlay = $('.screenArea').find('[data-itemno="'+t+'"]').data("delay")

            $('.lineSlider').animate({"marginLeft":"0"},0)
            $('.lineSlider').delay(1000).animate({"marginLeft":"435px"},dlay)
        });
    },
    draggableTools: function(){
        $('.control').draggable({
            axis:'y',
            cursor: 'move'
        });

        $('.topMenuContent1').draggable({containment:'parent',axis:'x'});
    },
//Text Methods

    dropDownFont:function(){
        var down = 1;
        $('.selectedFont').click(function(){
            down++;
            if(down%2 == 0)
                $('.fontDropDown').css({"overflowY":"scroll"}).animate({"height":"170px"})
            else
                $('.fontDropDown').css({"overflow":"hidden"}).animate({"height":"0px"})
        });

        $('.fontDropDown').find('.selectableFont').click(function(){
            down++;
            $('.fontDropDown').css({"overflow":"hidden"}).animate({"height":"0px"})
        });
    },

    dropDownSize:function(){
        var down = 1;
        $('.fontSize').click(function(){
                down++;
                if(down%2 == 0)
                $('.fontSizeSelection').css({"overflow":"hidden"}).animate({"height":"175px"})
                else
                $('.fontSizeSelection').css({"overflow":"hidden"}).animate({"height":"0px"})
        });

        $('.fontSizeSelection').find('.selectSize').click(function(){
            down++;
            $('.fontSizeSelection').css({"overflow":"hidden"}).animate({"height":"0px"})
        });
    },
    dropDownColor:function(){
        var down = 1;
        $('.fontColor').click(function(){
                down++;
                if(down%2 == 0)
                $('.fColorSel').css({"overflow":"hidden"}).animate({"height":"262px"})
                else
                $('.fColorSel').css({"overflow":"hidden"}).animate({"height":"0px"})
        });

        $('.screenArea').click(function(){
            down++;
            $('.fColorSel').css({"overflow":"hidden"}).animate({"height":"0px"})
        });
    },
    ddBG:1,
    ddRot:1,
    ddAlp:1,
    dropDownBG:function(){
        $('.colored').click(function(){
//                flapp.ddBG++;
            $('.topMenuTools').find('.tool').css({"overflow":"hidden"}).animate({"height":"0px"})
                if(flapp.ddBG == 1)
                $('.pallete').css({"overflow":"hidden"}).animate({"height":"263px"})
                else{
                    $('.pallete').css({"overflow":"hidden"}).animate({"height":"0px"})
                    flapp.ddBG = 0;
                }
        });

        $('.screenArea').click(function(){
            $('.pallete').css({"overflow":"hidden"}).animate({"height":"0px"})
            flapp.ddBG = 1;
        });
    },
    dropDownRotate:function(){
        $('.rotate').click(function(){
//            flapp.ddRot++;
            $('.topMenuTools').find('.tool').css({"overflow":"hidden"}).animate({"height":"0px"})
                if(flapp.ddRot == 1)
                $('.rotateTool').css({"overflow":"hidden"}).animate({"height":"50px"})
                else{
                    $('.rotateTool').css({"overflow":"hidden"}).animate({"height":"0px"})
                    flapp.ddRot = 0;
                }
        });

        $('.screenArea').click(function(){
            $('.rotateTool').css({"overflow":"hidden"}).animate({"height":"0px"})
            flapp.ddRot = 1;
        });
    },
    dropDownAlpha:function(){
        $('.resize').click(function(){
            $('.topMenuTools').find('.tool').css({"overflow":"hidden"}).animate({"height":"0px"})
                if(flapp.ddAlp == 1)
                $('.alphaTool').css({"overflow":"hidden"}).animate({"height":"50px"})
                else{
                    $('.alphaTool').css({"overflow":"hidden"}).animate({"height":"0px"})
                    flapp.ddAlp = 0;
                }
        });

        $('.screenArea').click(function(){
            $('.alphaTool').css({"overflow":"hidden"}).animate({"height":"0px"})
            flapp.ddAlp = 1;
        });
    },
    //Test
    rotate:function(){
        $('.rotateSlider').slider({
            min : -360,
            max : 360,
            value : 0,
            slide: function(){
                $(flapp.objectId).css('transform','rotate('+$('.rotateSlider').slider("value")+'deg)');
                $(flapp.objectId).data('rotate',$('.rotateSlider').slider("value")+'deg')
            }
        });

    },
    addOpacity:function(){
            var a;
            var b;
            var c;
            var color;
            $('.alphaSlider').slider({
                max:100,
                min:10,
                value:100,
                animate:true,
                start:function () {
                    a = $(flapp.objectId).css("background-color");
                    b = a.split("(");
                    color = b[1].replace(")", "");
                },
                slide:function (event, ui) {
                    a = $(flapp.objectId).css("background-color");
                    b = a.split("(");
                    c = b[1].replace(")", "");
                    color = c.split(",")
                    $(flapp.objectId).css({
                        'background':'rgba(' + color[0] + ',' + color[1] + ',' + color[2] + ',' + $('.alphaSlider').slider("value") / 100 + ')'
                    });
                    $(flapp.objectId).data("background",
                        'background','rgba(' + color[0] + ',' + color[1] + ',' + color[2] + ',' + $('.alphaSlider').slider("value") / 100 + ')'
                    );
                }
            });
    },
    addColor:function(){
//        $('.color').click(function(){
//            $(flapp.objectId).css("background","red");
//            $(flapp.objectId).data("background","red");
//        });
    },
    addRound:function(){
        $('.addRound').click(function(){
            $(flapp.objectId).css("borderRadius","50px");
            $(flapp.objectId).data("borderRadius","50px");
        });
    },


    //Set All event Calls here
    //Invoke This Function on page Load of appCreator.php
    setEvents:function(){
        flapp.addSquare();
        flapp.addCircle();
        flapp.addText();
//        flapp.editText();
        flapp.accessElem();
//        flapp.record();
//        flapp.stop();
        flapp.recordStop();
//        flapp.play();
        flapp.playAll();

        //Test
        flapp.rotate();
        flapp.draggableTools();
        flapp.addOpacity();
        flapp.addColor();
        flapp.addRound();
        flapp.uiStuff();

        //Tool Actions
        flapp.dropDownSize()
        flapp.dropDownFont()
        flapp.dropDownColor()
        flapp.dropDownRotate()
        flapp.dropDownAlpha()
        flapp.dropDownBG()
    }
}