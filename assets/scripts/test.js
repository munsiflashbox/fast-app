/**
 * @author Munsif
 */


//Main Function
(function ($) {
    $.fn.liveDraggable = function (opts) {
        this.live("mouseover", function () {
            if (!$(this).data("init")) {
                $(this).data("init", true).draggable(opts);
            }
        });
        return $();
    };
}(jQuery));

var zenoop = {};

zenoop.plusPage = 3;

zenoop.currPage = '';
zenoop.count_txt = 0;
zenoop.ImageCount = 0;

//Unused Variables
var pageCopy = 3;
var minusPage = 0;
var count = 0;
var counter = 0;
var resizeId = "";

//Editors Var's'
var oDoc, sDefTxt;
var Jqtemp = "";
var handlerId = "";
var this_id = "";
var temp = "";
var top_p = "";
var left_p = "";
var objContentEditable = 'false';

//Shape Counts
var squareCount = 0;
var rSquareCount = 0;
var lineCount = 0;
var dLineCount = 0;
var doLineCount = 0;
var selector = '';
var shadowColor = '';

//Menu vars
var menuId = '';
var menuCount = 3;
var menuTitle = '';
var menuAction = '';
var leftPos = '';
var topPos = '';

//Video Counters
var youTubeVideoCounter = 0;
var vimeoVideoCounter = 0;
var youTubeURL = '';
var vimeoURL = '';
var copyCount = 0;


var lineColor = '';
//Gallery Vars
var galleryCount = 0;

//var pathPreFix = "http://my.zenoop.com/zenoop-app/assets/images/";
var pathPreFix = "/assets/images/";

var oDoc, sDefTxt;
function initDoc(newId) {
    oDoc = document.getElementById(newId);
    sDefTxt = oDoc.innerHTML;
}

function formatDoc(sCmd, sValue) {
    document.execCommand(sCmd, false, sValue);
    oDoc.focus();
}

/*
 var menu = [];

 menu[0] = "<div class = 'menuItem' id = 'menuItems' alt = 'menu'>";
 menu[1] = "<ul class = 'navMenu'>";
 menu[2] += "<li class = 'menuItem1' alt = 'buildArea1'><a>Link 1.1</a></li>";
 menu[2] += "<li class = 'menuItem2' alt = 'buildArea2'><a>Link 1.2</a></li>";
 menu[2] +=	"<li class = 'menuItem3' alt = 'buildArea3'><a>Link 1.3</a></li>";
 menu[3] += "</ul>";
 menu[4] += "</div>"*/
///////////////////////////////////////////////////////////////////////////////

zenoop.addText = function () {
    $(function () {
        $('.txt_lib').click(function () {
            zenoop.count_txt++;
            $(zenoop.currPage + ' .content')
                .append("<div class = 'txtarea' style = 'z-index:" + zenoop.count_txt + "' onclick = 'initDoc(this.id)' id = 'textArea" + zenoop.count_txt + "' name = 'text' data-type-drag = 'ui-dragme' ><div class = 'txtHolder'  ></div></div>");

            this_id = '#textArea' + zenoop.count_txt;

            var cont = "<font color = '#5ba2d4' size = '3'><b>Welcome to Zenoop!</b></font><br/>";
            cont += "<font color = '#717171' size = '2'><b>To quickly locate topics on a particular subject in the documentation,</b></font><br/><br/>";
            cont += "Search field. Use the Search frame to display the Search view. You can narrow the scope of your search by selecting only the sections you are interested in.";
            cont += "To quickly locate topics on a particular subject in the documentation, enter a query in the ";
            cont += "Search field. Use the Search frame to display the Search view. You can narrow the scope of your search by selecting only the sections you are interested in.";
            cont += "To quickly locate topics on a particular subject in the documentation, enter a query in the ";
            cont += "Search field. Use the Search frame to display the Search view. You can narrow the scope of your search by selecting only the sections you are interested in.";
            cont += "To quickly locate topics on a particular subject in the documentation, enter a query in the ";
            cont += "Search field. Use the Search frame to display the Search view. You can narrow the scope of your search by selecting only the sections you are interested in.";
            cont += "To quickly locate topics on a particular subject in the documentation, enter a query in the ";
            cont += "Search field. Use the Search frame to display the Search view. You can narrow the scope of your search by selecting only the sections you are interested in.";
            cont += "To quickly locate topics on a particular subject in the documentation, enter a query in the ";
            cont += "Search field. Use the Search frame to display the Search view. You can narrow the scope of your search by selecting only the sections you are interested in.";

            $(this_id + ' .txtHolder').html(cont);
            $(this_id).resizable({
                handles:'n,s,e,w,ne,se,nw,sw',
                autoHide:true,
                resize:function (event, ui) {
                    $(this_id + ' .txtHolder').css({
                        "overflow":"hidden"
                    });
                }
            }).draggable({
                    cursor:'move'
                })
                .css({
                    "margin-top":"50px",
                    "z-index":zenoop.count_txt
                });

            zenoop.autoSave(this_id);
            zenoop.addEnter(this_id);
            zenoop.handler(this_id);
            zenoop.remHandler('.content');
        });
    });
}


zenoop.autoSave = function (elementId) {
    $(function () {
        $(elementId).on("blur", function () {
            //zenoop.blurElem(this.id);
            $data = {
                pageid:$(this).parent().parent().find('.pageid').val(),
                contents:$(this).parent().html()
            };

            $.ajax({
                url:"../index.php/appCreator/savePage",
                type:'POST',
                data:$data,
                success:function (msg) {
                    //alert(msg);
                }
            });
        });
    });
}

zenoop.remHandler = function (id) {
    $(function () {
        // $(zenoop.currPage+":not("+id+")").click(function () {
//
        // $('#handler').hide();
        // });
        $(id+":not("+zenoop.currPage+")").on('mousedown',function(){
            //alert('');
            $('#handler').removeClass('visible');
            $('#handler').hide();
        });
    });
}

zenoop.handler = function (divHandler) {
    $(zenoop.currPage).on('mouseup',divHandler,function () {
        //alert(zenoop.currPage);
        handlerId = "#" + $(this).attr('id');
        var pos_top_p = $(handlerId).offset().top - 26;
        var pos_left_p = $(handlerId).offset().left + 2;
        var this_width = $(handlerId).css("width");

        // $("#handler").css({
        // top:pos_top_p,
        // left:pos_left_p
        // });
        $('#handler').addClass('visible');
        $('#handler').attr('data-identifier', divHandler);
        $('#handler').css({
            top:pos_top_p,
            left:pos_left_p
        }).show();
    });
}

zenoop.handlerActions = function () {
    $(function () {

        $('#handler .zUp').click(function () {
            var this_index = parseInt($(handlerId).css("z-index"));
            var new_index = parseInt(this_index + 1);
            $(handlerId).css({
                "z-index":new_index
            });
        });

        $('#handler .zDwn').click(function () {
            //alert("Decrease Zindex By 1 for "+ handlerId);
            var this_index = parseInt($(handlerId).css("z-index"));
            var new_index = parseInt(this_index - 1);
            //alert(new_index);
            if (new_index < 0) {
                $(handlerId).css({
                    "z-index":1
                });
            } else {
                $(handlerId).css({
                    "z-index":new_index
                });
            }
        });

        $('#handler .closeHandler').click(function () {
            $(handlerId).remove();
            $('#handler, .editVideo, .editLine, .editShapes').fadeOut();
        });

        $('#handler .copyElement').mouseup(function () {
            //alert(handlerId);
            copyCount++;
            $(handlerId).clone().appendTo(zenoop.currPage + ' .content').attr('id', 'copy' + copyCount).draggable({
                cursor:'move'
            });
            if ($(handlerId).attr("alt") == "circle") {
                $('#copy' + copyCount).resizable({
                    handles:'',
                    autoHide:true,
                    aspectRatio:1
                })
            } else if ($(handlerId).attr("name") == "line") {
                $('#copy' + copyCount).resizable({
                    handles:'',
                    autoHide:true
                })
            } else if ($(handlerId).attr("name") == "youTube" || $(handlerId).attr("name") == "vimeo") {
                $('#copy' + copyCount).resizable({
                    handles:'',
                    maxWidth:960,
                    maxHeight:720
                })
            }
            else if($(handlerId).data("object") == "gallery"){
                zenoop.Gallery('#copy' + copyCount);
                zenoop.showTabs('#copy' + copyCount);
                zenoop.addImgToGal('#copy' + copyCount);
                $('#copy' + copyCount).resizable({
                    handles:''
                })
            }
            else {
                $('#copy' + copyCount).resizable({
                    handles:'',
                    autoHide:true
                })
            }
            $('#copy' + copyCount).css({
                "margin-top":"-10px",
                "margin-left":"10px"
            }).dblclick(function () {
                    $(this).draggable('disable');
                    $(this).attr("contenteditable", "true");
                    selector = $(this).attr("id");
                    objContentEditable = 'true';
                });

            // Call to Handler Function
            zenoop.handler('#copy' + copyCount);
        });

        $('#handler .hideHandler').click(function () {
            $('#handler').fadeOut();
            $('.editVideo').fadeOut();
            $('.editLine').fadeOut();
            $('.editShapes').fadeOut();
        });
    });
}

zenoop.fonts = function (id, value) {
    $(id).on('click', function () {
        formatDoc('fontname', value);
    });

}

zenoop.loadPage = function () {
    $(function () {
        $('#builderArea').find('.buildArea').hide();
        $('#buildArea1').show();
        zenoop.currPage = '#buildArea1';
    });
}

zenoop.fontSize = function (id, value) {
    $(id).on('click', function () {
        formatDoc('fontsize', value);
    });
}

zenoop.fontColor = function () {
    $('#colorPallete .levels .color').click(function () {
        //alert($(this).css("background-color"));
        //$('.selected').css({"background":$(this).css("background")});
        formatDoc('forecolor', $(this).css("background-color"));
    });
}
zenoop.textAlign = function (clickId, align) {
    $(function () {
        $(clickId).click(function () {
            formatDoc(align);
            if (align == "justifyfull")
                ;
            {
                $(handlerId).css({
                    "text-align":"justify"
                });
            }

        });
    });
}

zenoop.colorPicker = function () {
    $(function () {
        $('#fontColor').ColorPicker({
            onChange:function (hbs, hex, rgb) {
                objContentEditable = 'true';
                $(handlerId).attr('contenteditable', 'true');
                //alert(handlerId);
                //zenoop.getSelectionText();
                //$(handlerId).html($(this).html().replace(st,'<span>'+st+'</span>'));

            }
        });
    });
}

zenoop.showToolTips = function (id, text) {
    $(function () {
        $(id).hover(function () {
            $(this).mousemove(function (e) {
                $('.toolTip').show();
                $('.toolTip').html(text);
                $('.toolTip').css({
                    'left':e.pageX,
                    'top':e.pageY
                });
            });
        }, function () {
            $('.toolTip').hide();
        });
    });
}
//Blur Function
zenoop.blurElem = function (itemName) {
    objContentEditable = 'false';
    $('#' + selector).draggable('enable');
    $('#' + selector).resizable('enable');
    $('#' + selector).attr('contenteditable', 'false');
}
////// Draggables
zenoop.draggables = function (elementId) {
    $(function () {
        $(elementId).draggable({
            cursor:'move',
            iframeFix:true,
            scroll:false,
            stop:function () {

            }
        });
    });
}

zenoop.draggableRibbon = function (elementId) {
    $(function () {
        $(elementId).draggable({
            cursor:'move',
            containment:'parent'
        });
    });
}
//Adding Videos

zenoop.addyouTube = function (clickId) {
    $(function () {
        $(clickId).mouseup(function () {
            youTubeVideoCounter++;
            $('.youtubeVideo').clone()
                .removeClass('youtubeVideo')
                .addClass('youtubeVideoClone')
                .attr('id', "youTubeVideo" + youTubeVideoCounter)
                .appendTo(zenoop.currPage + ' .content')
                .resizable({
                    maxHeight:720,
                    maxWidth:960,
                    autoHide:true,
                    handles:'n,e,w,s,ne,nw,se,sw'
                }).draggable({
                    containment:zenoop.currPage + '.content',
                    iframeFix:true,
                    cursor:'move',
                    scroll:false
                });


            var frame = document.createElement('iframe');
            frame.src = 'http://youtube.com/embed/rPbACKftN3g'
            frame.width = '300';
            frame.height = '650';
            frame.frameborder = "0";
            frame.scrolling = "no";
            frame.marginHeight = "0";
            frame.marginWidth = "0";
            frame.vspace = "0";
            frame.hspace = "0";
            frame.allowTransparency = "true";
            frame.setAttribute('style', 'border:none; left:0, top: 0');
            //var youtubeDiv = document.getElementById("youTubeVideo" + youTubeVideoCounter);
            document.getElementById("youTubeVideo" + youTubeVideoCounter).appendChild(frame);

            /*  youTubeVideoCounter++;
             $('.youtubeVideo').clone()
             .removeClass('youtubeVideo')
             .addClass('youtubeVideoClone')
             .attr('id', "youTubeVideo" + youTubeVideoCounter).appendTo(zenoop.currPage + ' .content')
             .resizable({
             maxHeight:720,
             maxWidth:960,
             autoHide:true,
             handles:'n,e,w,s,ne,nw,se,sw'
             }).draggable({
             containment:zenoop.currPage + '.content',
             iframeFix:true,
             cursor:'move',
             scroll:false
             });
             $("#youTubeVideo" + youTubeVideoCounter).find('iframe').attr("src","http://www.youtube.com/embed/qM_NfA-NIXY?rel=0");
             */
            zenoop.handler('#youTubeVideo' + youTubeVideoCounter);
            // zenoop.dragAll({cursor:'move'});
            // zenoop.resizeAll({autoHide:true, handles:'se,sw,ne,nw,n,e,s,w'})

        });

    });
}

zenoop.addVimeo = function () {
    $(function () {
        $('.vimeoVids').mouseup(function () {
            vimeoVideoCounter++;
            $('.vimeoVideo').clone().removeClass('vimeoVideo').addClass('vimeoVideoClone').attr('id', "vimeoVideo" + vimeoVideoCounter).appendTo(zenoop.currPage + ' .content')
                .resizable({
                    maxHeight:720,
                    maxWidth:960,
                    autoHide:true,
                    handles:'n,e,w,s,ne,nw,se,sw'
                }).draggable({
                    containment:zenoop.currPage + '.content',
                    cursor:'move'
                });

            var frame = document.createElement('iframe');
            frame.src = 'http://player.vimeo.com/video/22973123'
            frame.width = '300';
            frame.height = '650';
            frame.webkitAllowFullScreen = true;
            frame.mozallowfullscreen = true;
            frame.allowFullScreen = true;
            frame.frameborder = "0";
            frame.scrolling = "no";
            frame.marginHeight = "0";
            frame.marginWidth = "0";
            frame.vspace = "0";
            frame.hspace = "0";
            frame.allowTransparency = "true";
            frame.setAttribute('style', 'border:none; left:0, top: 0');
            //var youtubeDiv = document.getElementById("youTubeVideo" + youTubeVideoCounter);
            document.getElementById("vimeoVideo" + vimeoVideoCounter).appendChild(frame);
            /*
             vimeoVideoCounter++;
             $('.vimeoVideo').clone()
             .removeClass('vimeoVideo')
             .addClass('vimeoVideoClone')
             .attr('id', "vimeoVideo" + vimeoVideoCounter)
             .appendTo(zenoop.currPage + ' .content')
             .resizable({
             maxHeight:720,
             maxWidth:960,
             autoHide:true,
             handles:'n,e,w,s,ne,nw,se,sw'
             }).draggable({
             containment:zenoop.currPage + '.content',
             cursor:'move'
             });
             $("#vimeoVideo" + vimeoVideoCounter).find('iframe').attr("src","http://player.vimeo.com/video/41369274");
             */
            zenoop.handler('#vimeoVideo' + vimeoVideoCounter);
        });
    });
}
///Editing Video URLs

zenoop.editVideo = function () {
    $(function () {
        $('.editVideo').mouseup(function () {
            if ($(handlerId).attr('name') == "youTube") {
                $('.youTubeURL').fadeIn();
                $('.youTubeButton').mouseup(function () {
                    if ($('#youTubeURLWriter').val() != "") {
                        $('.youtubeVideo .youTubeiframe').fadeOut(function () {
                            $(this).remove();
                        });
                        $('.youtubeVideo').append($('#youTubeURLWriter').val());
                        $('.youtubeVideo iframe').addClass("youTubeiframe");
                        $('.youtubeVideo').fadeIn();
                        $('.youTubeURL').fadeOut();
                    } else if ($('#youTubeURLWriter').val() == "") {
                        $('.popUp').fadeIn(function () {
                            $('.popUpmessage').text("Add a URL Please")
                        });
                        $('#youTubeURLWriter').focus()
                    }
                });
            } else if ($(handlerId).attr('name') == "vimeo") {
                $('.vimeoURL').fadeIn();
                $('.vimeoButton').mouseup(function () {
                    if ($('#vimeoURLWriter').val() != "") {
                        $('.vimeoVideo .vimeoiframe').fadeOut(function () {
                            $(this).remove();
                        });
                        $('.vimeoVideo').append($('#vimeoURLWriter').val());
                        $('.vimeoVideo iframe').addClass("vimeoiframe");
                        $('.vimeoVideo').fadeIn();
                        $('.vimeoURL').fadeOut();
                    } else if ($('#vimeoURLWriter').val() == "") {
                        $('.popUp').fadeIn(function () {
                            $('.popUpmessage').text("Add a URL Please")
                        });
                        $('#vimeoURLWriter').focus()
                    }
                });
            }

        });
    });
}
//Adding Shapes

zenoop.addLine = function () {
    $(function () {
        $('#stLine').click(function () {
            lineCount++;
            $(zenoop.currPage + " .content")
                .append("<div class = 'line' name = 'line' data-object = 'line' id = 'line" + lineCount + "' data-type = 'ui-responsible'></div>");
            $("#line" + lineCount).css({
                "height":"5px",
                'border-top':'solid',
                'border-top-width':'1px',
                'border-color': "#000000"
            }).draggable({
                    cursor:'move'
                }).resizable({
                    handles:'se',
                    maxWidth:'1020',
                    autoHide:true
                });

            zenoop.handler('#line' + lineCount);
            zenoop.remHandler('.content');
        });
    });
}

zenoop.addDotLine = function () {
    $(function () {
        $('#daLine').click(function () {
            dLineCount++;
            $(zenoop.currPage + " .content").append("<div class = 'dLine' name = 'line' data-object = 'dLine' id = 'dLine" + dLineCount + "' data-type = 'ui-responsible'></div>");
            $("#dLine" + dLineCount).css({
                "height":"5px",
                "border-top":"dashed",
                "border-top-width":"1px",
                'border-color': "#000000"
            });
            $('.dLine').draggable({
                cursor:'move'
            }).resizable({
                    maxWidth:'1020',
                    handles:'se',
                    maxHeight:5,
                    autoHide:true
                }).selectable();

            zenoop.handler('#dLine' + dLineCount);
            zenoop.remHandler('.content');
        });
    });
}
var flip = 0;
zenoop.lineHorizontal = function () {
    $(function () {
        $('.horizontal').mouseup(function () {

            var type = $(handlerId).data("object");
            var getWidth = $(handlerId).css("height");
            var getLineWidth = $(handlerId).css("border-right-width");

            $('.lineCP').find('input[type="radio"]').attr("checked","");
            $('.lineCP').find('.horizontal').attr("checked","checked");
            if(flip == 1){

                if(type == "line"){
                    $(handlerId).css({
                        "width":getWidth,
                        "height":"5px",
                        "border-right":"none",
                        "border-top":"solid",
                        "border-width":getLineWidth,
                        "border-color":lineColor
                    });
                }
                else if(type == "dLine"){
                    $(handlerId).css({
                        "width":getWidth,
                        "height":"5px",
                        "border-right":"none",
                        "border-top":"dashed",
                        "border-width":getLineWidth,
                        "border-color":lineColor
                    });
                }
            }
            else{
                $('.popUp').fadeIn(function () {
                    $('.popUpmessage').text("It's Horizontal Already");
                });
            }
            flip = 0;
        });

    });
}

zenoop.lineVertical = function () {
    $(function () {
        $('.vertical').mouseup(function () {
            var type = $(handlerId).data("object");
            var getHeight = $(handlerId).css("width");
            var getLineWidth = $(handlerId).css("border-top-width");

            $('.lineCP').find('input[type="radio"]').attr("checked","");
            $('.lineCP').find('.vertical').attr("checked","checked");
            if(flip == 0){
                if(type == "line"){
                    $(handlerId).css({
                        "width":"5px",
                        "height":getHeight,
                        "border-top":"none",
                        "border-right":"solid",
                        "border-width":getLineWidth,
                        "border-color":lineColor
                    });
                }
                else if(type == "dLine"){
                    $(handlerId).css({
                        "width":"5px",
                        "height":getHeight,
                        "border-top":"none",
                        "border-right":"dashed",
                        "border-width":getLineWidth,
                        "border-color":lineColor
                    });
                }
            }
            else{
                $('.popUp').fadeIn(function () {
                    $('.popUpmessage').text("It's Vertical Already");
                });
            }
            flip = 1;
        });

    });
}

zenoop.increaseLineHeight = function () {
    //  $(function () {
    $('.incLineWidth').on('mouseup',function () {
        if(flip == 0){
            //alert($(handlerId).css("border-top-width"));
            $(handlerId).css({
                "border-top-width":parseInt($(handlerId).css("border-top-width")) + 1
            });
            if (parseInt($(handlerId).css("border-top-width")) > 1) {
                $('.decLineWidth').removeAttr("disabled");
            } else {
                $('.decLineWidth').attr("disabled", "disabled");
            }
        }
        else if(flip == 1){
            //alert($(handlerId).css("border-right-width"));
            $(handlerId).css({
                "border-width":parseInt($(handlerId).css("border-right-width")) + 1
            });
            if (parseInt($(handlerId).css("border-right-width")) > 1) {
                $('.decLineWidth').removeAttr("disabled");
            } else {
                $('.decLineWidth').attr("disabled", "disabled");
            }
        }
    });
    //  });
}

zenoop.decreaseLineHeight = function () {
    $(function () {
        $('.decLineWidth').mouseup(function () {

            if(flip == 1){
                if ($(handlerId).css("border-right-width") > '1px') {
                    $(handlerId).css({
                        "border-width":parseInt($(handlerId).css("border-right-width")) - 1
                    })
                } else {
                    $('.popUp').fadeIn(function () {
                        $('.popUpmessage').text("Minimum thickness of a line can be 1px only");
                    });
                }
            }
            else if(flip == 0){
                if ($(handlerId).css("border-top-width") > '1px') {
                    $(handlerId).css({
                        "border-width":parseInt($(handlerId).css("border-top-width")) - 1
                    })
                } else {
                    $('.popUp').fadeIn(function () {
                        $('.popUpmessage').text("Minimum thickness of a line can be 1px only");
                    });
                }
            }

        });
    });
}

zenoop.addSquare = function () {
    $(function () {
        $('#addSquare').click(function () {
            squareCount++;
            $(zenoop.currPage + " .content").append("<div class = 'square' name = 'polygon' id = 'square" + squareCount + "' data-type = 'ui-responsible' data-type-drag = 'ui-dragme' data-type-resize = 'ui-resize'></div>");

            $("#square" + squareCount).css({
                "border":"solid",
                "border-width":"0px",
                "border-color":"#f5f5f5"
            });
            // $('.square').draggable().resizable({
            // //maxWidth : '1020',
            // handles:'n,e,w,s,ne,se,nw,sw',
            // autoHide:true
            // }).resizable("widget");

            zenoop.handler('#square' + squareCount);
            //zenoop.adDroppable('#square'+squareCount);
            //Reset Shapes CP
            zenoop.getShapesAttr('#square' + squareCount);
            zenoop.remHandler('#square' + squareCount);
            zenoop.dragAll({cursor:'move'});
            zenoop.resizeAll({autoHide :true, handles:'se,sw,ne,nw,n,e,s,w'})
        });

    });

}

zenoop.addCircle = function () {
    $(function () {
        $('#addRsquare').click(function () {
            rSquareCount++;
            $(zenoop.currPage + " .content").append("<div class = 'rSquare' name = 'polygon' alt = 'circle' id = 'rSquare" + rSquareCount + "' data-type = 'ui-responsible'></div>");
            $("#rSquare" + rSquareCount).css({
                "border-radius":parseInt($("#rSquare" + rSquareCount).css("width")) * 3
            }).css({
                    "border":"solid",
                    "border-width":"1px"
                });
            $('.rSquare').draggable().resizable({
                aspectRatio:1,
                //maxWidth : '1020',
                handles:'n,e,w,s,ne,se,nw,sw',
                autoHide:true
            });
            zenoop.handler('#rSquare' + rSquareCount);
            //zenoop.adDroppable('#rSquare'+rSquareCount);
            zenoop.getShapesAttr('#rSquare' + rSquareCount);
            zenoop.remHandler('.content');
        });

    });
}
//Edit PopUp
zenoop.editPopup = function () {
    $(function () {
        $('#editIt').mouseup(function () {
            if ($(handlerId).attr("alt") == "menu") {
                //alert("line");
                $("#menuEditor, .screen").fadeIn();
                menuFuncs.scroller('linkManager');
            }
            if ($(handlerId).attr("name") == "text") {
                zenoop.showTextRibbon();
            }
            if ($(handlerId).attr("name") == "line") {
                $(".lineCP,.screen").fadeIn();
            }
            if ($(handlerId).attr("name") == "polygon") {
                $(".shapeControlPannel, .screen").fadeIn();
            }
            if ($(handlerId).attr("name") == "youTube") {
                $(".youTubeURL, .screen").fadeIn();
                //zenoop.showVideoRibbon(this);
            }
            if ($(handlerId).attr("name") == "vimeo") {
                $(".vimeoURL, .screen").fadeIn();
            }
        });
    });
}

zenoop.createLink = function(){
    var sLnk=prompt('Write the URL here','http:\/\/');
    if(sLnk && sLnk!='' && sLnk!='http://')
    {
        formatDoc('createlink',sLnk)
    }
}

zenoop.showTextRibbon = function () {
    $(function () {
        //$(clickId).click(function() {
        $('.texTit table').fadeOut(function () {
            $('.titText').html("Text");
            $('.imageCap img').attr("src", pathPreFix + "text_active_03.png");
        });
        $('.texTit table').fadeIn();

        $('#eribbonHolder').fadeOut(function () {
            $('.eRibbonNew').show();
            $('.vidRibbon').hide();
            $('.lineRibbon').hide();
            $('.shapesRibbon').hide();
            $('.galleryRibbon').hide();
        });
        $('#eribbonHolder').fadeIn();
    });
    //});
}

zenoop.showVideoRibbon = function (clickId) {
    $(function () {
        $(clickId).click(function () {
            $('.texTit table').fadeOut(function () {
                $('.titText').html("Video");
                $('.imageCap img').attr("src", pathPreFix + "video_active_03.png");
            });
            $('.texTit table').fadeIn();

            $('#eribbonHolder').fadeOut(function () {
                $('.eRibbonNew').hide();
                $('.lineRibbon').hide();
                $('.vidRibbon').show();
                $('.shapesRibbon').hide();
                $('.galleryRibbon').hide();
            });
            $('#eribbonHolder').fadeIn();

        });
        zenoop.addyouTube(clickId);
    });
}

zenoop.showGalleryRibbon = function () {
    $(function () {
        $('.gal_lib').click(function () {
            $('.texTit table').fadeOut(function () {
                $('.titText').html("Gallery");
                $('.imageCap img').attr("src", pathPreFix + "gallery_active_03.png");
            });
            $('.texTit table').fadeIn();

            $('#eribbonHolder').fadeOut(function () {
                $('.galleryRibbon').show();
                $('.shapesRibbon').hide();
                $('.lineRibbon').hide();
                $('.eRibbonNew').hide();
                $('.vidRibbon').hide();
            });
            $('#eribbonHolder').fadeIn();

        });
    });
}

zenoop.showShapeRibbon = function () {
    $(function () {
        $('.shapes').click(function () {
            $('.texTit table').fadeOut(function () {
                $('.titText').html("Shapes");
                $('.imageCap img').attr("src", pathPreFix + "shapes_active_03.png");
            });
            $('.texTit table').fadeIn();

            $('#eribbonHolder').fadeOut(function () {
                $('.shapesRibbon').show();
                $('.lineRibbon').hide();
                $('.eRibbonNew').hide();
                $('.vidRibbon').hide();
                $('.galleryRibbon').hide();
            });
            $('#eribbonHolder').fadeIn();

        });
    });
}

zenoop.showLineRibbon = function () {
    $(function () {
        $('.lines').click(function () {
            $('.texTit table').fadeOut(function () {
                $('.titText').html("Lines");
                $('.imageCap img').attr("src", pathPreFix + "lines_active_03.png");
            });
            $('.texTit table').fadeIn();

            $('#eribbonHolder').fadeOut(function () {
                $('.lineRibbon').show();
                $('.eRibbonNew').hide();
                $('.vidRibbon').hide();
                $('.shapesRibbon').hide();
                $('.galleryRibbon').hide();
            });
            $('#eribbonHolder').fadeIn();

        });
    });
}
//Adding a new page

zenoop.pageAdder = function () {
    zenoop.plusPage++;
    zenoop.currPage = '#buildArea' + zenoop.plusPage;
    var page = "<input type='hidden' name='pageid' value='00000000-0000-0000-0000-000000000000' class='pageid'/>";
    page += "<input type='hidden' name='userid' value='" + $('.active .userid').val() + "' class='userid' />";
    page += "<input type='hidden' name='appid' value='" + $('.active .appid').val() + "' class='appid' />";
    page += "<div class = 'content'>";
    /*page += "<div class = 'header'>";
     page += "<div class = 'zenoop.currPage'>";
     page += "page"+zenoop.plusPage;
     page += "</div>";
     page += "<div class = 'addPag'>";
     page += "<input type = 'text' id = 'textBox' class = 'text' />";
     page +=	"</div>";
     page += "<div class = 'actions'>";
     page += "<button class = 'edit'></button>";
     page += "<button class = 'addNewPage'></button>";
     page += "</div>";
     page += "<div class = 'clear'></div>";
     page += "</div>";*/
    page += "<div class = 'menuItem' id = 'menuItems' alt = 'menu' data-type = 'ui-drag'><ul data-type='menu' class = 'navMenu'></ul></div>";
    page += "</div>";

    //var menu = "<div class = 'menuItem' id = 'menuItems' alt = 'menu'><ul data-type='menu' class = 'navMenu'></ul></div>";

    $("#builderArea").append("<div id = 'buildArea" + zenoop.plusPage + "' class = 'buildArea' data-type='page'></div>");
    $("#buildArea" + zenoop.plusPage).append(page);

    $('#builderArea .buildArea').hide();
    $('#builderArea .buildArea').removeClass('active');
    $("#buildArea" + zenoop.plusPage).show();
    $("#buildArea" + zenoop.plusPage).addClass('active');
    $('#handler').hide();
}

zenoop.addPageHolder = function () {
    var pageHolder = "<div class = 'pageDiv' id = 'pagDiv" + zenoop.plusPage + "' data-page = 'buildArea" + zenoop.plusPage + "'><div class = 'pageDivBG' id = 'pagDiv" + zenoop.plusPage + "'><div>Page " + zenoop.plusPage + "</div></div></div>";

    $('#pageHolder .anyClass .overview').append(pageHolder);

}

zenoop.addPage = function (addPage) {
    $(function () {
        $(addPage).click(function () {
            //if ($('.active .appid').val() !== '00000000-0000-0000-0000-000000000000' && $('.active .apgeid').val() === '00000000-0000-0000-0000-000000000000') {
            if (common.appId !== '00000000-0000-0000-0000-000000000000' && $('.active .pageid').val() === '00000000-0000-0000-0000-000000000000') {
                $data = {
                    /*appid : $('.active .appid').val(),
                     userid : $('.active .userid').val()*/
                    appid:common.appId,
                    userid:common.userId
                };
                $.post('../index.php/appCreator/addPage', $data, function (msg) {
                });
            }// else if ($('.active .appid').val() !== '00000000-0000-0000-0000-000000000000' && $('.active .apgeid').val() !== '00000000-0000-0000-0000-000000000000') {
            else if (common.appId !== '00000000-0000-0000-0000-000000000000' && $('.active .pageid').val() !== '00000000-0000-0000-0000-000000000000') {
                $data = {
                    /*appid : $('.active .appid').val(),
                     userid : $('.active .userid').val()*/
                    appid:common.appId,
                    userid:common.userId
                };
                $.post('../index.php/appCreator/addPage', $data, function (msg) {
                    var x = eval('(' + msg + ')');
                    $('#buildArea' + $('.buildArea').length).find('.pageid').val(x);
                });
            }
            zenoop.pageAdder();
            menuFuncs.addMainMenuItem();
            zenoop.draggableMenu();
            menuFuncs.scroller('#linkManager');
            zenoop.addPageHolder();
            menuFuncs.scroller('#pageHolder .anyClass');
        });
    });
}


zenoop.deletePage = function () {
    $(function () {
        $('.deletePageholder').click(function () {
            //alert(zenoop.currPage);
            if (zenoop.currPage == '#buildArea1') {
                $(zenoop.currPage).next().show();
            } else {
                $(zenoop.currPage).prev().show();
            }
            $(zenoop.currPage).remove();
            //DeletePage();
            menuFuncs.scroller('#pageHolder .anyClass');
        });
    });
}

zenoop.duplicatePage = function () {
    $(function () {
        $('.copyPage').click(function () {
            zenoop.plusPage++;
            $(zenoop.currPage).clone().appendTo('#builderArea').attr("id", "buildArea" + zenoop.plusPage);
            // .find('.content .header .zenoop.currPage').append("-Copy");

            menuFuncs.addMainMenuItem();
            zenoop.draggableMenu();
            zenoop.addPageHolder();
            menuFuncs.scroller('#linkManager');
            menuFuncs.scroller('#pageHolder .anyClass');

            $('#builderArea .buildArea').hide();
            $('#builderArea .buildArea').removeClass('active');
            $("#buildArea" + zenoop.plusPage).show();
            $("#buildArea" + zenoop.plusPage).addClass('active');
        });
    });
}

zenoop.alignMenu = function () {
    $(function () {
        var width = $(window).width();

        if (width > 1280 && width < 1400) {
            $('#menuBox').css({
                "left":"80px",
                "display":"block"
            });
        } else if (width > 1400 && width < 1600) {
            $('#menuBox').css({
                "left":"140px",
                "display":"block"
            });
        } else if (width > 1600 && width < 2000) {
            $('#menuBox').css({
                "left":"250px",
                "display":"block"
            });
        } else if (width < 1300) {
            $('#menuBox').css({
                "left":"50px",
                "display":"block"
            });
        }
    });
}
//zenoop.alignMenu();

//Shapes Color and Sliders
zenoop.boxShadowColor = function () {
    $(function () {
        $('.shadowColor').ColorPicker({
            onChange:function (hsb, hex, rgb) {
                $('.shadowColor').css({
                    "backgroundColor":"#" + hex
                });
                shadowColor = "#" + hex;
                $(handlerId).css({
                    "boxShadow":"0px 0px " + $('#shadowSlider').slider("value") + "px " + shadowColor
                });
            }
        });
    });
}

zenoop.boxFillColor = function () {
    $(function () {
        $('.fillColor').ColorPicker({
            onChange:function (hsb, hex, rgb) {
                $('.fillColor').css({
                    "backgroundColor":"#" + hex
                });
                $(handlerId).css({
                    "background":"#" + hex
                });
            }
        });
    });
}

zenoop.boxBorderColor = function () {
    $(function () {
        $('.borColor').ColorPicker({
            onChange:function (hsb, hex, rgb) {
                $('.borColor').css({
                    "backgroundColor":"#" + hex
                });
                $(handlerId).css({
                    "border-color":"#" + hex
                });
            }
        });
    });
}

zenoop.lineBorderColor = function () {
    $(function () {
        $('.lineColor').ColorPicker({
            onChange:function (hsb, hex, rgb) {
                $('.lineColor').css({
                    "backgroundColor":"#" + hex
                });
                lineColor = '#'+hex;
                $(handlerId).css({
                    "border-color":lineColor
                });
            }
        });
    });
}

zenoop.borderWidth = function () {
    $(function () {
        $('#borderThickness').slider({
            max:100,
            min:0,
            animate:true,
            slide:function (event, ui) {
                $(handlerId).css({
                    'border-width':$('#borderThickness').slider("value")
                });
            }
        });
    });
}

zenoop.borderRadius = function () {
    $(function () {
        $('#borderRoundness').slider({
            max:$(handlerId).css("width"),
            min:0,
            animate:true,
            slide:function (event, ui) {
                if ($(handlerId).attr("alt") != "circle") {
                    $(handlerId).css({
                        'border-radius':$('#borderRoundness').slider("value")
                    })
                } else {
                    $('.popUp').fadeIn(function () {
                        $('.popUpmessage').text("You cannot apply Border Radius for a circle");
                    })
                }
            }
        });
    });
}

zenoop.opacity = function () {
    $(function () {
        $('#opacitySlider').slider({
            max:100,
            min:10,
            value:100,
            animate:true,
            slide:function (event, ui) {
                $(handlerId).css({
                    'opacity':$('#opacitySlider').slider("value") / 100
                });
            }
        });
    });
}

zenoop.shadowSlider = function () {
    $(function () {
        $('#shadowSlider').slider({
            max:100,
            min:0,
            animate:true,
            slide:function (event, ui) {
                $(handlerId).css({
                    "boxShadow":"0px 0px " + $('#shadowSlider').slider("value") + "px " + shadowColor
                });
            }
        });
    });
}

zenoop.getShapesAttr = function (id) {
    $(function () {
        $(id).mouseup(function () {
            var a = $(id).css("box-shadow");
            var b = a.replace(/ /g, "_");
            var c = b.split("_");
            var d = parseInt(c[5]);
            var shaColor = c[0] + c[1] + c[2];
            //alert(shaColor);
            var rad = parseInt($(id).css("borderTopLeftRadius"));
            //Fetching BgColor
            $(".fillColor").css({
                "background":$(id).css("background-color")
            });
            //Fetching borColor
            $(".borColor").css({
                "background":$(id).css("border-top-color")
            });
            //Fetching ShadowColor
            shadowColor = shaColor;
            $('.shadowColor').css({
                "background":shadowColor
            });

            if (rad > 1) {
                $("#borderRoundness").slider({
                    value:rad
                });
            } else {
                $("#borderRoundness").slider({
                    value:0
                });
            }

            //Fetch Border-width
            $("#borderThickness").slider({
                value:parseInt($(id).css("border-top-width"))
            });

            if (d > 1) {
                $("#shadowSlider").slider({
                    value:d
                });

            } else {
                $("#shadowSlider").slider({
                    value:0
                });
                //shadowColor = "rgb(0,0,0)";
            }

            //Fetch Opacity
            $('#opacitySlider').slider({
                value:$(id).css("opacity") * 100
            });

        });
    });
}

zenoop.addTextShade = function () {
    $(function () {
        if ($(handlerId).hasClass("textShadow")) {
            $(handlerId).removeClass("textShadow");
        } else {
            $(handlerId).addClass("textShadow");
        }
    });
}
//Drag and Clone Image
zenoop.cloneImage = function () {
    $(function () {
//
        // $(zenoop.currPage + ' .content').droppable({
        // drop : function(){
        // $().append();
        // zenoop.dragAll();
        // zenoop.resizeAll({autoHide:true,handles: 'se,sw,ne,nw,n,e,s,w'});
        // }
        // });

        $('.dragme').draggable({
            helper:'clone',
            cursor:'move',

            stop:function (e) {
                if($('.visible').length)
                {
                    var ElementCount = $('#streamImage').find('li').length;
                    //var Pos = $('#streamImage').offset();
                    //alert(Pos.left);
                    var Pos = document.getElementById('streamImage').offsetTop;
                    //fetch offset value
                }else{
                    zenoop.ImageCount++;
                    $(this).clone().css({
                        "height":"300",
                        "width":"300",
                        "position":"absolute",
                        "z-index":zenoop.ImageCount,
                        "margin-top":"50px"
                    }).appendTo(zenoop.currPage + ' .content')
                        .removeClass('dragme')
                        .addClass('cloned')
                        .attr("id", "cloned" + zenoop.ImageCount)
                        .attr("data-type-drag","ui-dragme")
                        .attr("data-type-resize","ui-resize");

                    zenoop.handler("#cloned" + zenoop.ImageCount);
                    zenoop.remHandler('.content');
                    zenoop.dragAll();
                    zenoop.resizeAll({autoHide:true,handles: 'se,sw,ne,nw,n,e,s,w'});
                }
            }
        });
    });
}

//Show popups
zenoop.showPopUps = function (clickId, actionId) {
    $(function () {
        $(clickId).click(function () {
            $(actionId).fadeIn();
            $('.screen').fadeIn();
        });
    });
}

zenoop.showPopRec = function (clickId, actionId) {
    $(function () {
        $(clickId).click(function () {
            $(actionId).fadeIn();
            $('.screen').fadeIn();
            $('#signInBox, #signUpBox').fadeOut();
        });
    });
}

zenoop.showsignPopUps = function (clickId, actionId) {
    $(function () {
        $(clickId).click(function () {
            $(actionId).fadeIn();
            $('.screen').fadeIn();
            $('#preStart').fadeOut();
        });
    });
}

zenoop.showsignPopOuts = function (clickId) {
    $(function () {
        $(clickId).click(function () {
            $('#signInBox, #signUpBox, #recoverEmail').fadeOut();
            $('.screen').fadeIn();
            $('#preStart').fadeIn();
        });
    });
}

zenoop.showImgPopUps = function (clickId, actionId) {
    $(function () {
        $(clickId).click(function () {
            $(actionId).fadeIn();
            getAllImages();
        });
    });
}
//Hide popups
zenoop.closePopUps = function (clickId, actionId) {
    $(function () {
        $(clickId).click(function () {
            $(actionId).fadeOut();
            $('.screen').fadeOut();
        });
    });
}

zenoop.closePopUp = function (clickId, actionId) {
    $(function () {
        $(clickId).click(function () {
            $(actionId).fadeOut();
        });
    });
}
//KeyClose PopUp
zenoop.closePopUpsKey = function () {
    $(function (e) {
        if (e.keyCode == 13) {
            $('.popUp').fadeOut();
            return false;
        }
    });
}

zenoop.cyclePages = function () {
    $(function () {
        $('#builderArea').cycle({
            fx:'',
            speed:500,
            timeout:0,
            prev:'#pagePrev',
            next:'#pageNext'
        });
    });
}
/*
 zenoop.closeAllButMe = function() {
 $(function() {
 $('ul').on('li','click',function(){
 var closingId = $(this).attr("alt");
 alert('#'+closingId);
 alert(zenoop.currPage);
 $('#builderArea .buildArea').fadeOut();
 $('#builderArea .buildArea').removeClass('active');
 $('#' + closingId).fadeIn();
 $('#' + closingId).addClass('active');
 zenoop.currPage = '#' + closingId;
 });
 });
 }
 */


zenoop.closeAllButMe = function (clickId) {
    $(function () {
        $(clickId).live('click', function () {
            var closingId = $(this).data("page");
            var menuCls = closingId.replace("buildArea", "menuItem");
            var pagCls = closingId.replace("buildArea", "pagDiv");
            //alert('#'+closingId);

            $('#builderArea .buildArea').hide();
            $('#builderArea .buildArea').removeClass('active');

            $('#' + closingId).show();
            $('#' + closingId).addClass('active');

            $('.navMenu').find('a').attr("class", "");
            $('.navMenu').find('.' + menuCls + ' a').addClass('ahover');

            $('#pageHolder').find('.pageDiv').attr("class", "pageDiv");
            $('#pageHolder').find('.pageDiv[data-page = "' + closingId + '"]').addClass('phover');

            zenoop.currPage = '#' + closingId;
            //	blockPage(closingId);
        });
    });
}
//
// zenoop.closeAllButMe = function (clickId) {
// $(function () {
// $(clickId).live('click', function () {
// var closingId = $(this).data("page");
// var menuCls = closingId.replace("buildArea", "menuItem");
// var pagCls = closingId.replace("buildArea", "pagDiv");
// //alert('#'+closingId);
//
// $('#builderArea .buildArea').hide();
// $('#builderArea .buildArea').removeClass('active');
//
// $('#' + closingId).show();
// $('#' + closingId).addClass('active');
//
// $('.navMenu').find('a').attr("class", "");
// $('.navMenu').find('.' + menuCls + ' a').addClass('ahover');
//
// $('#pageHolder').find('.pageDiv').attr("class", "pageDiv");
// $('#pageHolder').find('.pageDiv[data-page = "' + closingId + '"]').addClass('phover');
//
// zenoop.currPage = '#' + closingId;
// //	blockPage(closingId);
// });
// });
// }

zenoop.adDroppable = function (Id) {
    $(function () {
        $(Id).droppable({
            drop:function (event, ui) {
                var $this = $(this);
                $this.append(ui.draggable);

                $(ui.draggable).css({
                    "z-index":parseInt($(this).css("z-index")) + 1
                });
            }
        });
    });
}

zenoop.remDroppable = function () {
    $(function () {
        $('.content').droppable({
            drop:function (event, ui) {
                var $this = $(this);
                $this.append(ui.draggable);
            }
        });
    });
}

zenoop.dropDowns = function () {
    $(function () {
        $('.coCreatorsHolder').hover(function () {
            $('.coCreateDD').slideDown();
        }, function () {
            $('.coCreateDD').slideUp();
        });
    });
}

zenoop.addEnter = function (textId) {
    $(function () {

        $(textId).dblclick(function () {
            $(this).attr("contenteditable", "true");
            $(this).draggable('disable');
            selector = $(this).attr("id");
            objContentEditable = 'true';

        });

        $(textId).blur(function () {
            $(this).attr('contenteditable', 'false');
            $(this).draggable('enable');
        });

        //Adding Enter or Break Space
        $(textId).live("keyup",function () {
            if (!this.lastChild || this.lastChild.nodeName.toLowerCase() != "br") {
                $(this).append("<br/>");
            }
        }).live("keypress", function (e) {
                if (e.which == 13) {
                    if (window.getSelection) {
                        var selection = window.getSelection(), range = selection.getRangeAt(0), br = document.createElement("br");
                        range.deleteContents();
                        range.insertNode(br);
                        range.setStartAfter(br);
                        range.setEndAfter(br);
                        range.collapse(false);
                        selection.removeAllRanges();
                        selection.addRange(range);
                        return false;
                    }
                }
            });

    });
}

zenoop.sampleEditor = function () {
    $(function () {
        $('#sampleContentEditable').dblclick(function () {
            $(this).attr("contenteditable", "true");
        });

        $('#sampleContentEditable').blur(function () {
            $(this).attr('contenteditable', 'false');
        });

        $("#sampleContentEditable")

            // make sure br is always the lastChild of contenteditable
            .live("keyup mouseup", function () {
                if (!this.lastChild || this.lastChild.nodeName.toLowerCase() != "br") {
                    this.appendChild(document.createChild("br"));
                }
            })
            // use br instead of div div
            .live("keypress", function (e) {
                if (e.which == 13) {
                    if (window.getSelection) {
                        var selection = window.getSelection(), range = selection.getRangeAt(0), br = document.createElement("br");
                        range.deleteContents();
                        range.insertNode(br);
                        range.setStartAfter(br);
                        range.setEndAfter(br);
                        range.collapse(false);
                        selection.removeAllRanges();
                        selection.addRange(range);
                        return false;
                    }
                }
            });

    });
}

zenoop.adDroppable = function (Id) {
    $(function () {
        $(Id).droppable({
            drop:function (event, ui) {
                var $this = $(this);
                $this.append(ui.draggable);

                $(ui.draggable).css({
                    "z-index":parseInt($(this).css("z-index")) + 1
                });
            }
        });
    });
}

//Adding Video URL's

zenoop.addYouTubeURL = function (clickId) {
    $(function () {
        $('.youTubeButton').mouseup(function () {
            //alert(handlerId);
            if ($('#youTubeURLWriter').val() != "") {
                $(handlerId + ' .youTubeiframe').fadeOut(function () {
                    $(this).remove();
                });
                $(handlerId).append($('#youTubeURLWriter').val());
                $(handlerId + ' iframe').addClass("youTubeiframe");
                $('.youtubeVideo').fadeIn();
                $('.youTubeURL, .screen').fadeOut();
            } else if ($('#youTubeURLWriter').val() == "") {
                $('.popUp').fadeIn(function () {
                    $('.popUpmessage').text("Add a URL Please")
                });
                $('#youTubeURLWriter').focus()
            }
        });
    });
}

zenoop.addYouTubeSRC = function (clickId) {
    $(function () {
        $('.youTubeButton').mouseup(function () {
            //alert(handlerId);
            if ($('#youTubeURLWriter').val() != "") {
                $(handlerId + ' iframe').attr("src", $('#youTubeURLWriter').val());
                $('.youTubeURL, .screen').fadeOut();
            } else if ($('#youTubeURLWriter').val() == "") {
                $('.popUp').fadeIn(function () {
                    $('.popUpmessage').text("Paste a URL in the text feild please")
                }).delay(3000).fadeOut();
                ;
                $('#youTubeURLWriter').focus()
            }
        });
    });
}

zenoop.addVimeoURL = function (clickId) {
    $(function () {
        $('.vimeoButton').mouseup(function () {
            //alert(handlerId);
            if ($('#vimeoURLWriter').val() != "") {
                $(handlerId + ' .vimeoiframe').fadeOut(function () {
                    $(this).remove();
                });
                $(handlerId).append($('#vimeoURLWriter').val());
                $(handlerId + ' iframe').addClass("vimeoeiframe");
                $('.vimeoVideo').fadeIn();
                $('.vimeoURL, .screen').fadeOut();
            } else if ($('#vimeoURLWriter').val() == "") {
                $('.popUp').fadeIn(function () {
                    $('.popUpmessage').text("Paste a URL in the text feild please")
                }).delay(3000).fadeOut();
                $('#vimeoURLWriter').focus()
            }
        });
    });
}

zenoop.draggableMenu = function () {
    $(function () {
        $('.menuItem').draggable({
            stop:function (event, ui) {
                leftPos = $(this).css("left");
                topPos = $(this).css("top");
                for (var loopVar = 0; loopVar < zenoop.plusPage + 1; loopVar++) {
                    $('#buildArea' + loopVar).find(".menuItem").css({
                        "top":topPos,
                        "left":leftPos
                    })
                }
                //alert(leftPos);
                //alert(rightPos);
            }
        });
    });
}

zenoop.previewMode = function () {
    $(function () {
        $('.snapButton').draggable({
            snap:'.edit',
            snapTolerance:50,
            containment:'#modes',
            scroll:true,
            drag:function (event, ui) {
                var left = $('.snapButton').offset().left;
                var right = $('.snapButton').offset().right;

                if(left > right){
                    $('.snapButton').animate({
                        "float":"left"
                    });
                }
            }
        });

        $('.edit').droppable({
            drop:function (event, ui) {
                $('.snapButton').html("Edit").animate({
                    "margin-left":"80px;"
                });
                $('.bar, .leftPannel, #menuBox, #pageHolder').show();
                $('.buildArea').find('[data-type="ui-drag"]').draggable({
                    disabled:false
                });
                $('.buildArea').find('[data-type="ui-responsible"]').draggable({
                    disabled:false
                }).resizable({
                        disabled:false
                    });
                $('.buildArea').find('[data-type-drag="ui-dragme"]').draggable({
                    disabled:false
                }).resizable({
                        disabled:false
                    });
                $('#mainWrapper').animate({
                    "margin-top":"0px"
                })
            }
        });
        $('.preview').droppable({
            drop:function (event, ui) {
                $('.snapButton').html("Preview");
                $('.bar, .leftPannel, #menuBox, #pageHolder, .coat').hide();
                $('.buildArea').find('[data-type="ui-drag"]').draggable({
                    disabled:true
                });
                $('.buildArea').find('[data-type="ui-responsible"]').draggable({
                    disabled:true
                }).resizable({
                        disabled:true
                    });
                $('.buildArea').find('[data-type-drag="ui-dragme"]').draggable({
                    disabled:true
                }).resizable({
                        disabled:true
                    });
                $('#mainWrapper').animate({
                    "margin-top":"-75px"
                });
                $('#handler').hide();
                objContentEditable = 'true';
            }
        });
    });

    $('.searchHolder').find('.search_btn').click(function () {
        var sh = new search();
        //alert($('.search_string').val());
        sh.app($('.search_string').val());
    });

}
var imageArray ='';
zenoop.Gallery = function (idNo) {
    $(function () {
        imageArray = $(idNo).find("#streamImage img").map(function () {
            return $(this).attr("src");
        });
        console.log(imageArray);
        var timeDelay = 3000;
        var i = 0;
        var timer = '';

        //function timeOutMe(){
        timer = $.timer(function () {
            //console.log('The value of I is :' + idNo);
            $(idNo).find('#galImage').fadeOut(function(){
                $(this).css({
                    "background":"url(" + imageArray[i] + ")",
                    "background-size":"cover"
                })
            }).fadeIn();

            if (i == imageArray.length - 1) {
                i = 0;
            } else {
                i++;
            }
        })
        timer.set({
            time:timeDelay,
            autostart:true
        });
        //}
        $(idNo).find('#streamImage li img').live('click',function () {
            if (timer) {
                timer.pause();
                var ind = $(this);
                $(idNo).find('#galImage').fadeOut(function(){
                    $(this).css({
                        "background":"url(" + imageArray[i] + ")",
                        "background-size":"cover"
                    })
                }).fadeIn();

                setTimeout(function () {
                    i = parseInt($(ind).data('index'), 10);
                    if (i == imageArray.length - 1) {
                        i = 0;
                    }
                    timer.play();
                }, 2000);
            }
        });

        $('.gallerella').draggable({
            cursor:'move'
        }).resizable({
                autoHide:true,
                handles:"n,e,s,w,ne,nw,se,sw",
                minWidth:"400",
                minHeight:"300"
            });
    });
}

zenoop.addGallery = function () {
    $(function () {
        $('.gal_lib').click(function () {
            galleryCount++;
            $('.gallery').clone()
                .appendTo(zenoop.currPage + ' .content')
                .attr("class", "gallerella")
                .css({
                    "position":"absolute",
                    "margin-top":"50px",
                    "z-index":galleryCount
                })
                .show()
                .attr("id", "galleryNo" + galleryCount)
                //.attr("data-page","galleryNo"+galleryCount)
                .draggable({
                    cursor:'move'
                }).resizable({
                    autoHide:true,
                    handles:"n,e,w,s,ne,nw,se,sw",
                    minWidth:"400",
                    minHeight:"300"
                });

            zenoop.Gallery('#galleryNo' + galleryCount);
            zenoop.showTabs('#galleryNo' + galleryCount);
            zenoop.handler('#galleryNo' + galleryCount);
            zenoop.remHandler('.content');
            zenoop.addImgToGal('#galleryNo' + galleryCount);
            // $('.galTabs').jcarousel({
            // vertical:false,
            // scroll:1
            // });
        });

    });
}

zenoop.addImgToGal = function(id){
    $(function(){
        $(id).droppable({
            drop:function(event,ui){
                var ElementCount = $('#streamImage').find('li').length;
                var srce = $(ui.draggable).css("background-image");
                var a = srce.split('"');
                var image = "<li> <img src="+a[1]+" data-index="+ElementCount+"></li>";
                //alert(image);
                $(id).find('#streamImage')
                    .append(image);
                $(id).find(ui.draggable).remove();
                imageArray = $(id).find("#streamImage img").map(function () {
                    return $(this).attr("src");
                });
                if($('.visible').length){
                    $('#streamImage').find('li:last').addClass('found');
                }
            }
        });
    });
}

zenoop.dragAll = function(opts){
    $(function(){
        // alert("fond it");
        $('[data-type-drag="ui-dragme"]').draggable(opts);
    });
}

zenoop.resizeAll = function(opts){
    $(function(){
        //alert("fond it");
        $('[data-type-resize="ui-resize"]').resizable(opts);
    });
}


zenoop.showTabs = function (idNo) {
    $(function () {
        $(idNo).find('.hotSpot').click(function () {
            $(idNo).find('.galHoder').slideToggle();
        });
        $(idNo).find('.galHoder').mouseleave(function () {
            $(idNo).find('.galHoder').slideToggle();
        });
    });
}

zenoop.Fontize = function () {
    $(function () {
        $('.fonts').on('click', function () {
            formatDoc('fontname', $(this).css("font-family"));
        });
    });
}

zenoop.setEvents = function () {
    zenoop.Fontize();
    //zenoop.remDroppable();
    ////////////////////////////TEST AREA//////////////////////////
    //zenoop.cyclePages();

    zenoop.loadPage();

    //zenoop.Gallery();
    zenoop.addGallery();

    zenoop.previewMode();
    zenoop.draggableMenu();
    zenoop.sampleEditor();

    zenoop.dropDowns();

    zenoop.closeAllButMe('#menuItems ul li');
    zenoop.closeAllButMe('#pageHolder .anyClass .pageDiv');

    ////////////////////////////////////////////Functions Call back//////////////////////////////////////

    zenoop.addText();
    zenoop.handlerActions();

    //Add font family
    zenoop.fonts('#font1', 'courier new');
    zenoop.fonts('#font2', 'Arial');
    zenoop.fonts('#font3', 'Droid Sans');
    zenoop.fonts('#font4', 'Times New Roman');
    zenoop.fonts('#font5', 'Chiller');
    zenoop.fonts('#font6', 'Verdana');
    zenoop.fonts('#font7', 'tahoma');

    //Add Font Sizes
    zenoop.fontSize('#size1', '1');
    zenoop.fontSize('#size2', '2');
    zenoop.fontSize('#size3', '3');
    zenoop.fontSize('#size4', '4');
    zenoop.fontSize('#size5', '5');
    zenoop.fontSize('#size6', '6');
    zenoop.fontSize('#size7', '7');

    //FontColor
    //zenoop.colorPicker();
    //zenoop.fontColor();

    //Text ALignment
    zenoop.textAlign('#leftAlign', 'justifyleft');
    zenoop.textAlign('#rightAlign', 'justifyright');
    zenoop.textAlign('#centerAlign', 'justifycenter');
    zenoop.textAlign('#fullAlign', 'justifyfull');

    //Tool Tips for text Editor
    zenoop.showToolTips('.LogoMiyan', 'Zenoop!');
    zenoop.showToolTips('#bold', 'Bold');
    zenoop.showToolTips('#italic', 'Italic');
    zenoop.showToolTips('#fontSize', 'Font size');
    zenoop.showToolTips('#fontColor', 'Font color');
    zenoop.showToolTips('#leftAlign', 'Left align');
    zenoop.showToolTips('#centerAlign', 'Center align');
    zenoop.showToolTips('#rightAlign', 'Right align');
    zenoop.showToolTips('#fullAlign', 'Justify');
    zenoop.showToolTips('#link', 'Link');
    zenoop.showToolTips('#tShade', 'Text shadow');

    //Page Holder Tool Tip
    zenoop.showToolTips('.copyPage', 'Duplicate a Page');
    zenoop.showToolTips('.deletePageholder', 'Delete Page');
    zenoop.showToolTips('.addPageholder', 'Add Page');

    //ToolTips for Menu Bar
    zenoop.showToolTips('.img_lib', 'Add Image');
    zenoop.showToolTips('.txt_lib', 'Add Text');
    zenoop.showToolTips('.vid_lib', 'Add Video');
    zenoop.showToolTips('.gal_lib', 'Add Gallery');
    zenoop.showToolTips('.addPage', 'Add Page');
    zenoop.showToolTips('.share', 'Share');
    zenoop.showToolTips('.extra', 'Extras');
    zenoop.showToolTips('.lines', 'Add Line');
    zenoop.showToolTips('.shapes', 'Add Shape');
    zenoop.showToolTips('.coCreate', 'Invite Friends');
    zenoop.showToolTips('.open', 'Open Project');
    zenoop.showToolTips('.addLink', 'Add Link');
    zenoop.showToolTips('.save', 'Save Project');

    //ToolTips for Handler
    zenoop.showToolTips('.zUp', 'Shift Up');
    zenoop.showToolTips('.zDwn', 'Shift Down');
    zenoop.showToolTips('.hideHandler', 'Hide Handler');
    zenoop.showToolTips('.copyElement', 'Copy');
    zenoop.showToolTips('.closeHandler', 'Delete');
    zenoop.showToolTips('#editIt', 'Edit');

    //Tool Tip for Page Holder
    zenoop.showToolTips('#stripLace', 'Open/Close Page Holder');
    //ToolTips for Lines
    zenoop.showToolTips('#stLine', 'Straight Line');
    zenoop.showToolTips('#daLine', 'Dotted Line');

    //ToolTips for Shapes
    zenoop.showToolTips('#addSquare', 'Add Square');
    zenoop.showToolTips('#addRsquare', 'Add Circle');

    //ToolTips for Vidoes
    zenoop.showToolTips('.youTubeVids', 'Add youTube Video');
    zenoop.showToolTips('.vimeoVids', 'Add vimeo Video');

    //ToolTips for Gallery
    zenoop.showToolTips('.addGallery', 'Add gallery with tabs');
    zenoop.showToolTips('.noTabsGallery', 'Add gallery without tabs');

    //////Draggables

    //Video
    // zenoop.draggables('#vimeoURL');
    // zenoop.draggables('#youTubeURL');
//
    // //all Draggables
    // zenoop.draggables('#menuBox');
    // zenoop.draggables('.coat');
    // zenoop.draggables('.imageGallery');
    // zenoop.draggables('.menuControlPannel');
    // zenoop.draggables('.shapeControlPannel');
    // zenoop.draggables('#menuEditor');
    // zenoop.draggables('#signInBox');
    // zenoop.draggables('#signUpBox');
    // zenoop.draggables('.vimeoURL');
    // zenoop.draggables('.youTubeURL');
    // //zenoop.draggables('.menuItem');
    // zenoop.draggables('#galSlidePics');
//
    zenoop.draggableRibbon('.eRibbonNew');
//
    // zenoop.draggables('#projectSettings');
    // //PopUp Draggable
//
    // zenoop.draggables('.popUp');
//
    // zenoop.draggables('#pageSettingPannel');

    zenoop.dragAll({cursor:'move'});
    zenoop.resizeAll({autoHide: true, handles : 'se,sw,ne,nw,n,e,s,w'});

    //Adding a Page
    zenoop.addPage('.addPage');
    zenoop.addPage('.addPageholder');

    //Duplicating a page
    zenoop.duplicatePage();

    //Deleting a Page
    zenoop.deletePage();

    //Adding Videos
    zenoop.addyouTube('.youTubeVids');
    zenoop.addVimeo();

    //Editing Videos
    zenoop.editVideo();

    //Adding Shapes and Lines
    zenoop.addLine();
    zenoop.addDotLine();
    zenoop.addCircle();
    zenoop.addSquare();

    //Toggle menu Ribbon
    zenoop.showTextRibbon();
    zenoop.showVideoRibbon('.vid_lib');
    zenoop.showGalleryRibbon();
    zenoop.showLineRibbon();
    zenoop.showShapeRibbon();

    //Edit Popups
    zenoop.editPopup();

    //Show PopUps
    zenoop.showPopUps('.editLine', '.lineCP');
    zenoop.showImgPopUps('.img_lib', '.coat');


    zenoop.showPopRec('.signUpElements a', '#recoverEmail');
    zenoop.draggables('#recoverEmail');

    //cloning a Image
    zenoop.cloneImage();

    //Hide Popups

    zenoop.closePopUp('.closeGal', '.coat');
    zenoop.closePopUp('.popUpButton', '.popUp');
    zenoop.closePopUps('.menuEditorClose', '#menuEditor');
    zenoop.closePopUps('.closeMenuEditor', '#menuEditor');

    //Close Video popups
    zenoop.closePopUps('#closeVimeo', '.vimeoURL');
    zenoop.closePopUps('#closeYouTube', '.youTubeURL');

    //Embedding Video URL's'
    zenoop.addYouTubeURL();
    zenoop.addVimeoURL();

    //SignUp / In
    zenoop.closePopUps('.resetbtn', '#signInBox');
    zenoop.closePopUps('.resetbtnUp', '#signUpBox');

    zenoop.closePopUps('#closeAppName', '#appNamePopUp');

    //Pre Start
    zenoop.closePopUps('#closePreStart', '#preStart');
    zenoop.closePopUps('#tryZenoop', '#preStart');
    zenoop.showsignPopUps('#signInZenoop', '#signInBox');
    zenoop.showsignPopUps('#signUpZenoop', '#signUpBox');
    zenoop.showsignPopOuts('.resetbtn');
    zenoop.showsignPopOuts('.closeRecbtn');
    zenoop.showsignPopOuts('.resetbtnUp');

    zenoop.closePopUps('#closeSignIn', '#signInBox');
    zenoop.closePopUps('#closeSignUp', '#signUpBox');

    zenoop.closePopUps('#closeRecover', '#recoverEmail');
//	zenoop.closePopUps('.closeRecbtn', '#recoverEmail');

    zenoop.closePopUpsKey();

    //Shapes CP PopUps
    zenoop.closePopUps('.closeShapeCP', '.shapeControlPannel');
    zenoop.closePopUps('.btnCloseShapeCP', '.shapeControlPannel');

    //Close Open projects
    zenoop.closePopUps('#closeOpen', '#openProject');

    zenoop.closePopUps('.closeLineCP', '.lineCP');
    zenoop.closePopUps('#closeLineEditor', '.lineCP');

    //Shapes colors and Sliders
    zenoop.boxBorderColor();
    zenoop.boxFillColor();
    zenoop.boxShadowColor();
    zenoop.lineBorderColor();

    zenoop.borderRadius();
    zenoop.borderWidth();
    zenoop.shadowSlider();
    zenoop.opacity();

    //Add text Shadow
    zenoop.addTextShade();

    //Line Controls
    zenoop.lineHorizontal();
    zenoop.lineVertical();
    zenoop.increaseLineHeight();
    zenoop.decreaseLineHeight();

    zenoop.handler('.menuItem');

    /*function getAllPages(appid){
     $data = {
     Appid: appid
     };
     $.post('../index.php/appCreator/pageCount', $data, function(msg){
     return msg;
     });
     }*/

    $('.pageid').parent().addClass('active');

    $('.newBtn .text').blur(function () {
        $data = {
            //userid:$('.userid').val(),
            userid:common.userId,
            appname:$('#sample').val(),
            description:'TestDescription'
        };

        $.ajax({
            url:'../index.php/Application/createApp',
            type:'POST',
            data:$data,
            success:function (msg) {
                var x = eval('(' + msg + ')');
                // $('.appid').val(x);
                common.appId = x;
                CreateDefaultPage();
            }
        });
    });

    $('#buttons1').click(function () {
        $pageid = $('.active').find('.pageid').val();

        if ($pageid != '00000000-0000-0000-0000-000000000000') {
            $data = {
                pageid:$('.active').find('.pageid').val()
            };
            //alert($('.active').find('.pageid').val());
            $.ajax({
                url:'../index.php/appCreator/deletePage',
                type:'POST',
                data:$data,
                success:function (msg) {
                    window.location = '#';
                }
            });
            //window.location = '../../deletePage';
        }
    });

    $('.actions .button').click(function () {
        alert('');
    });

    $('#contentArea').draggable({
        stop:function () {
            $data = {
                pageid:$('#pageControls').find('.pageid').val(),
                contents:JSON.stringify($('#contentArea').html())
            };

            $.ajax({
                url:"../index.php/appCreator/savePage",
                type:'POST',
                data:$data,
                success:function (msg) {
                    //alert($('#contentAreas').html());
                }
            });
        }
    });

    $('.txtHolder').blur(function () {
        $data = {
            pageid:$('#pageControls').find('.pageid').val(),
            contents:JSON.stringify($('#contentArea').html())
        };

        $.ajax({
            url:"../index.php/appCreator/savePage",
            type:'POST',
            data:$data,
            success:function (msg) {
                //alert($('#contentAreas').html());
            }
        });
    });

    /*$('.pageHolderFooter').find('.deletePageholder').click(function(){
     var action = $(this).attr('class');

     $data = {
     pageid: $('#pageControls').find('.pageid').val()
     };

     $.ajax({
     url: "../appCreator/deletePage",
     type: 'POST',
     data: $data,
     success: function(msg){
     alert(msg);
     }
     });
     });*/

    /*$('#pageControls').find('.savePage').click(function(){
     var action = $(this).attr('class');

     $data = {
     pageid: $('#pageControls').find('.pageid').val(),
     contents: JSON.stringify($('#contentArea').contents())
     };

     if($('#pageControls').find('.pageid').val() == null)
     {
     alert('');
     }

     $.ajax({
     url: "../../appCreator/savePage",
     type: 'POST',
     data: $data,
     success: function(msg){
     alert(msg);
     }
     });
     });*/

    $('.publish .publishPage').click(function () {
        //$appid = $('.active .appid').val();
        $appid = common.appId;
        if ($('.active .userid').val() == '00000000-0000-0000-0000-000000000000') {
            $('#signInBox').show('slow');
        } else if ($appid == '00000000-0000-0000-0000-000000000000') {

            $('#appNamePopUp').show('slow');
        } else {
            $data = {};

            $.ajax({
                url:'../index.php/Application/publish/' + encode64($('.active .appid').val() + '/1'),
                type:'POST',
                data:$data,
                success:function (msg) {
                    //alert(msg);
                    $('.publish').find('.publishPage').hide();
                }
            });
        }
    });

    $('#setAppName').click(function () {
        $('#appNamePopUp').hide('slow');
        $appdata = {
            //userid:$('.userid').val(),
            userid:common.userId,
            appname:$('#appNamePopUp .thisContent input').val(),
            description:'TestDescription'
        };

        $.ajax({
            url:'../index.php/Application/createApp',
            type:'POST',
            data:$appdata,
            success:function (msg) {
                var x = eval('(' + msg + ')');
                $('.appid').val(x);
                publishApp();
            }
        });
    });

    function publishApp() {
        $data = {};

        $.ajax({
            url:'../index.php/Application/publish/' + encode64($('.active').find('.appid').val()),
            type:'POST',
            data:$data,
            success:function (msg) {
                $('.publish').find('.publishPage').hide();
            }
        });
    }


    $('.open').click(function () {
        $data = {
            //userid:$('.active').find('.userid').val()
            userid:common.userId
        };
        $.ajax({
            url:'../index.php/appCreator/getAllApps',
            type:'POST',
            data:$data,
            success:function (msg) {
                $('#openProject').show('slow');
                var x = eval('(' + msg + ')');
                $result = '';
                for (var i = 0; i < x.length; i++) {
                    $result = $result + '<div class = "projects even"><div class = "projName even">' + x[0].Name + '</div><div class = "pagesStatus">12 Pages<span class = "status">Recent</span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
                }
                $('#openProject').find('.projectHolder').html($result);
            }
        });
    });

    function CreateDefaultPage() {
        $data = {
            /*appid:$('.active .appid').val(),
             userid:$('.active .userid').val(),*/
            appid:common.appId,
            userid:common.userId,
            page:$('.buildArea').length
        };

        $.post('../index.php/appCreator/editPage', $data, function (msg) {
            var x = eval('(' + msg + ')');
            for (var i = 0; i < x.length; i++) {
                $('#buildArea' + (i + 1)).find('.pageid').val(x[i].Id);
                saveContent(i + 1);
            }

        });

    }

    function saveContent(val) {
        /*for(var j = 1; j < $('.buildArea').length; j++)
         {
         $data = {
         pageid: $('#buildArea'+j).find('.pageid').val(),
         contents: $('#buildArea'+j).find('.content').html()
         };
         $.post('../index.php/appCreator/savePage', $data, function(msg){
         alert(j);
         });
         }*/

        $data = {
            pageid:$('#buildArea' + val).find('.pageid').val(),
            contents:$('#buildArea' + val).find('.content').html()
        };
        $.post('../index.php/appCreator/savePage', $data, function (msg) {
            //alert(val+'<br/>'+$data.contents);
        });
        //$('#buildArea"'+val+'"').find('.content').addClass(val);
    }

}

$(function () {
    $('#openProject .projectHolder').find('.projects').live('click', function () {
        $('#openProject').hide('slow');
        //$('.appid').val($(this).attr('data-appid'));
        common.appId = $(this).data('appid');
        $data = {
            //appid : $(this).attr('data-appid')
            appid:common.appId
        };

        $.post('../index.php/appCreator/getAllPages', $data, function (msg) {
            //var x = eval('(' + msg + ')');
            var x = JSON.parse(msg);
            for (var i = 3; i < x.length; i++) {
                zenoop.pageAdder();
                menuFuncs.addMainMenuItem();
                zenoop.draggableMenu();
                menuFuncs.scroller('#linkManager');
                zenoop.addPageHolder();
            }

            var PageCount = x.length;
            if (PageCount < 1) {
                console.log('Page Not Found');
                $('.popUp').fadeIn(function () {
                    $('.popUpmessage').text("Page Not Found For This Project");
                });
            } else {
                var j = 0;
                $('.buildArea').each(function () {
                    $(this).find('.pageid').val(x[j].Id);
                    $(this).find('.content').html(x[j].PageContent);
                    j++;
                });

                var k = 0;
                $('.viewport .pageDiv').each(function () {
                    $(this).attr('data-pageid', x[k].Id);
                    $(this).find('.pageDivBG div').text(x[k].PageName);
                    k++;
                });
            }
            $('.screen').hide();
        });
    });
});

function openProjPopup() {

    //   alert("me");
    this.open = function () {
        $('#openProject').show('slow');
    }

    this.getRecentProj = function (id, SortOrder) {
        $data = {
            userid:id,
            SortOrder:SortOrder
        };

        $.ajax({
            url:'../index.php/appCreator/getAllApps/',
            type:'POST',
            data:$data,
            success:function (msg) {
                var x = JSON.parse(msg);
                $result = $('.projectHolder').html();
                if (SortOrder === 'all') {
                    // $(x).each(function(idx,ele){
                    // });
                    for (var i = 0; i < x.length; i++) {
                        $result = $result + '<div class = "projects even" data-appid="' + x[i].Id + '"><div class = "projName even">' + x[i].Name + '</div><div class = "pagesStatus"><span class="pagecount">&nbsp;</span><span class = "status"></span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
                    }
                } else {
                    for (var i = 0; i < x.length; i++) {
                        //$result = $result + '<div class = "projects even" data-appid="'+x[i].Id+'"><div class = "projName even">'+x[i].Name+'</div><div class = "pagesStatus"><span class="pagecount">0</span> Pages<span class = "status">Recent</span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
                        $result = $result + '<div class = "projects even" data-appid="' + x[i].Id + '"><div class = "projName even">' + x[i].Name + '</div><div class = "pagesStatus"><span class="pagecount">&nbsp;</span><span class = "status">Recent</span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
                        //getAllPages(x[i].Id);
                    }
                }
                $('#openProject').find('.projectHolder').html($result);
            }
        });
    }

    this.BuildMenu = function () {
        $data = {
            //Appid : $('.active .appid').val()
            Appid:common.appId
        };

        $.post('../index.php/appCreator/pageCount', $data, function (msg) {
            //alert(msg);
        });
    }

}


$(function () {
    $('.search_string').keyup(function () {
        //$('#suggestions').show();
        $data = {
            appname:$('.search_string').val(),
            //userid : $('.active .userid').val()
            userid:common.userId
        };

        $.post('../index.php/Application/searchApp', $data, function (msg) {
            //var x = eval('(' + msg + ')');
            var x = JSON.parse(msg);
            $('#openProject').find('.projectHolder').html('');
            $result = '';
            for (var i = 0; i < x.length; i++) {
                $result = $result + '<div class = "projects even" data-appid="' + x[i].Id + '"><div class = "projName even">' + x[i].Name + '</div><div class = "pagesStatus"><span class="pagecount">&nbsp;</span><span class = "status">&nbsp;</span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
            }
            $('#openProject').find('.projectHolder').html($result);
        });
    });

    $(window).bind('beforeunload', function () {
        $('.buildArea').each(function () {
            $data = {
                pageid:$(this).find('.pageid').val(),
                contents:$(this).find('.content').html()
            };

            $.ajax({
                url:"../index.php/appCreator/savePage",
                type:'POST',
                data:$data,
                success:function (msg) {
                    alert(msg);
                }
            });
        });
    });

    this.open = function () {
        $('#openProject').show('slow');
    }

    this.getRecentProj = function (id, SortOrder) {
        $data = {
            userid:id,
            SortOrder:SortOrder
        };

        $.ajax({
            url:'../index.php/appCreator/getAllApps/',
            type:'POST',
            data:$data,
            success:function (msg) {
                //var x = eval('(' + msg + ')');
                var x = JSON.parse(msg);
                $result = $('.projectHolder').html();
                if (SortOrder === 'all') {
                    // $(x).each(function(idx,ele){
                    // });
                    for (var i = 0; i < x.length; i++) {
                        $result = $result + '<div class = "projects even" data-appid="' + x[i].Id + '"><div class = "projName even">' + x[i].Name + '</div><div class = "pagesStatus"><span class="pagecount">&nbsp;</span><span class = "status"></span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
                    }
                } else {
                    for (var i = 0; i < x.length; i++) {
                        //$result = $result + '<div class = "projects even" data-appid="'+x[i].Id+'"><div class = "projName even">'+x[i].Name+'</div><div class = "pagesStatus"><span class="pagecount">0</span> Pages<span class = "status">Recent</span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
                        $result = $result + '<div class = "projects even" data-appid="' + x[i].Id + '"><div class = "projName even">' + x[i].Name + '</div><div class = "pagesStatus"><span class="pagecount">&nbsp;</span><span class = "status">Recent</span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
                        //getAllPages(x[i].Id);
                    }
                }
                $('#openProject').find('.projectHolder').html($result);
            }
        });
    }

    this.BuildMenu = function () {
        $data = {
            //Appid : $('.active .appid').val()
            Appid:common.appId
        };

        $.post('../index.php/appCreator/pageCount', $data, function (msg) {
            //alert(msg);
        });
    }
});

function search() {
    this.app = function (val) {
        $data = {
            appname:val,
            //userid : $('.active .userid').val()
            userid:common.userId
        };

        $.post('../index.php/Application/searchApp', $data, function (msg) {
            //var x = eval('(' + msg + ')');
            var x = JSON.parse(msg);
            $('#openProject').find('.projectHolder').html('');
            for (var i = 0; i < x.length; i++) {
                $result = '<div class = "projects even" data-appid="' + x[i].Id + '"><div class = "projName even">' + x[i].Name + '</div><div class = "pagesStatus"><span class="pagecount">&nbsp;</span><span class = "status">Recent</span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
            }
            $('#openProject').find('.projectHolder').html($result);
        });
    }
}


$(function () {
    $('.search_string').keyup(function () {
        //$('#suggestions').show();
        $data = {
            appname:$('.search_string').val(),
            //userid : $('.active .userid').val()
            userid:common.userId
        };

        $.post('../index.php/Application/searchApp', $data, function (msg) {
            //var x = eval('(' + msg + ')');
            var x = JSON.parse(msg);
            $('#openProject').find('.projectHolder').html('');
            $result = '';
            for (var i = 0; i < x.length; i++) {
                $result = $result + '<div class = "projects even" data-appid="' + x[i].Id + '"><div class = "projName even">' + x[i].Name + '</div><div class = "pagesStatus"><span class="pagecount">&nbsp;</span><span class = "status">Recent</span></div><div class = "edit"><input type = "button" value = "Edit"/></div><div class = "clear"></div></div>';
            }
            $('#openProject').find('.projectHolder').html($result);
        });
    });

    $(window).bind('beforeunload', function () {
        $('.buildArea').each(function () {
            $data = {
                pageid:$(this).find('.pageid').val(),
                contents:$(this).find('.content').html()
            };
            console.log($data.pageid);
            $.ajax({
                url:"../index.php/appCreator/savePage",
                type:'POST',
                data:$data,
                success:function (msg) {
                    alert(msg);
                }
            });
        });
    });

});

//block current page for other users///
function blockPage(id) {
    $data = {
        pageid:$('#' + id).find('.pageid').val(),
        //userid : $('#' + id).find('.userid').val()
        userid:common.userId
    };
    //alert($data.pageid);

    $.ajax({
        url:"../index.php/appCreator/blockCurrentPage",
        type:'POST',
        data:$data,
        success:function (msg) {
            //alert(msg);
        }
    });
}

function CreateDefaultPage() {
    $data = {
        appid:common.appId,
        userid:common.userId,
        page:$('.buildArea').length
    };

    $.post('../index.php/appCreator/editPage', $data, function (msg) {
        var x = eval('(' + msg + ')');
        console.log($data.appid);
        for (var i = 0; i < x.length; i++) {
            $('#buildArea' + (i + 1)).find('.pageid').val(x[i].Id);
            saveContent(i + 1);
        }

    });

}

function getAllImages() {
    $data = {
        appId:common.appId
    };

    $.post('../index.php/image/getImages', $data, function (msg) {
        var x = JSON.parse(msg);
        if(common.appId == '00000000-0000-0000-0000-000000000000'){
            $('#thumbImages').append("<li><div class='dragme' style='background:url(../assets/images/demoImages/01.jpg) no-repeat center; background-size: cover;'></div></li>");
            $('#thumbImages').append("<li><div class='dragme' style='background:url(../assets/images/demoImages/03.jpg) no-repeat center; background-size: cover;'></div></li>");
            $('#thumbImages').append("<li><div class='dragme' style='background:url(../assets/images/demoImages/04.jpg) no-repeat center; background-size: cover;'></div></li>");
            $('#thumbImages').append("<li><div class='dragme' style='background:url(../assets/images/demoImages/12.jpg) no-repeat center; background-size: cover;'>></div></li>");
            $('#thumbImages').append("<li><div class='dragme' style='background:url(../assets/images/demoImages/15.jpg) no-repeat center; background-size: cover;'></div></li>");
        }
        else{
            $('#thumbImages li').each(function(){
                $(this).remove();
            });
            for (var i = 0; i < x.length; i++) {

                //alert(x[i].Url);
                //  $('#thumbImages').append("<li><div data-type = 'ui-responsible' class = 'dragme ui-draggable' style='background:url(" + x[i].Url + ")no-repeat center;background-size: cover;float: left;  ' id = 'drag' ></div>");

                //console.log(x[i].Url);
                $('#thumbImages').append("<li><div class = 'dragme' style='background:url(" + x[i].Url + ")no-repeat center;background-size: cover'></div></li>");

            }
        }
        zenoop.cloneImage();
        $('#builderArea').find('[data-type="ui-dragResize"]').resizable({
            disabled:false,
            autoHide:true
        }).draggable({
                disabled:false,
                cursor:'move'
            });
        $('#thumbImages').jcarousel({
            vertical:true,
            scroll:1
        });
    });

}

$(document).ready(function(){
    $.post('../index.php/User/IsAuthenticated',function(msg){
        var result = JSON.parse(msg);
        if(result.status === 'success'){
            common.userId = result.data;
            $('#preStart').hide();
            $('#openProject').show();
            var popup = new openProjPopup();
            popup.open(result.data);
            popup.getRecentProj(result.data, 'recent');
            popup.getRecentProj(result.data, 'all');
        }
        else{
            $('#preStart').show();
        }
    });
});

var common = {};
common.userId = '00000000-0000-0000-0000-000000000000';
common.appId = '00000000-0000-0000-0000-000000000000';
common.page = [];
